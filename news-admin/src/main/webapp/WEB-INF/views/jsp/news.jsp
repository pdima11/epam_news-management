<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page session="true"%>

<html>
<head>
	<title>${fullNews.news.title}</title>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/news.css">>
</head>
<body>
    <div class="content">
        <div class="news-block">
            <div align="left">
                <div class="news-title">
                    ${fullNews.news.title}
                </div>
                <div class="news-author">(by ${fullNews.author.name})</div>
                <div class="news-date">
                    <fmt:formatDate pattern="dd/MM/yyyy" value="${fullNews.news.creationDate}" />
                </div>
            </div>
            <div>
                <pre style=" white-space: pre-wrap; ">${fullNews.news.fullText}</pre>
            </div>
        </div>

        <div class="comments-block">
            <c:forEach items="${fullNews.comments}" var="comment">
                <div class="comment-box">
                    <form action="${pageContext.request.contextPath}/delete-comment" method="POST" id="delete-comment">
                        <input type="hidden"
                               name="newsId" value="${fullNews.news.id}">

                        <input type="hidden" name="commentId" value="${comment.id}">

                        <input type="hidden" name="${_csrf.parameterName}"
                               value="${_csrf.token}" />

                        <input type="submit" class="remove-button" value="X">
                    </form>
                    <div class="comment-date">
                        <fmt:formatDate pattern="dd/MM/yyyy" value="${comment.creationDate}" />
                    </div>
                    <br>
                    <p class="comment-message">
                       ${comment.text}
                    </p>
                </div>
            </c:forEach>

            <div class="add-comment">

                <form:form action="${pageContext.request.contextPath}/add-comment" method="POST" modelAttribute="newComment">
                    <form:errors path="text" cssClass="error-input"/>
                    <form:textarea path="text" maxlength="500"/>
                    <form:hidden path="newsId" value="${fullNews.news.id}"/>

                    <input type="submit" value="Post Comment"/>
                </form:form>
            </div>
        </div>



    </div>
</body>
</html>