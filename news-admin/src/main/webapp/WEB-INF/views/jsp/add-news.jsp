<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>${fullNews.news.title}</title>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/addnews.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/multiple-select/multiple-select.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/multiple-select/multiple-select.js"></script>
</head>
<body>
    <div class="content">

        <form:form action="${pageContext.request.contextPath}/add-new-news" method="POST" modelAttribute="news"
                   cssClass="news-block">
            <div class="news-field" align="left">
                <div class="title">Title: </div>
                <form:errors path="title" cssClass="error-input"/>
                <form:input path="title" cssClass="input-title" maxlength="500"/>
            </div>
            <div class="news-field" align="left">
                <div class="title">Date: </div>
                <input id="date" type="text" class="input-date"/>
            </div>
            <div class="news-field" align="left">
                <div class="title">Brief: </div>
                <form:errors path="shortText" cssClass="error-input"/>
                <form:textarea path="shortText" cssClass="input-brief" maxlength="1000"/>
            </div>
            <div class="news-field" align="left">
                <div class="title">Content: </div>
                <form:errors path="fullText" cssClass="error-input"/>
                <form:textarea path="fullText" cssClass="input-content" maxlength="1500"/>
            </div>
            <div class="news-field">
                <select id="author-list" name="authorId" class="select-author"
                        required data-placeholder="Authors">
                    <c:forEach items="${authorList}" var="currentAuthor">
                        <c:if test="${currentAuthor.expiredDate == null}">
                            <option value="${currentAuthor.id}">${currentAuthor.name}</option>
                        </c:if>
                    </c:forEach>
                </select>
                <select id="tag-list" name="tagIdList" multiple="multiple" class="select-tag">
                    <c:forEach items="${tagList}" var="currentTag">
                        <option value="${currentTag.id}">${currentTag.name}</option>
                    </c:forEach>
                </select>
            </div>
            <input type="submit" value="Save" class="submit-button "/>

        </form:form>
    </div>
</body>
</html>


<script>
    $(document).ready(function() {
        $('#date').prop( "disabled", true );

        var now = new Date();
        var curr_date = now.getDate();
        var curr_month = now.getMonth() + 1;
        var curr_year = now.getFullYear();

        $('#date').val(curr_date + '/' + curr_month + '/' + curr_year);
    });

    $('#tag-list').multipleSelect();
</script>
