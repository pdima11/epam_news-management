<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="menu">
	<div>
		<div class="nav">
			<a class="menu-element" href="${pageContext.request.contextPath}/news-list">News List </a>
		</div>
		<div class="nav">
			<a class="menu-element" href="${pageContext.request.contextPath}/add-news">Add news</a>
		</div>
		<div class="nav">
			<a class="menu-element" href="${pageContext.request.contextPath}/author-list">Add/Update authors</a>
		</div>
		<div class="nav">
			<a class="menu-element" href="${pageContext.request.contextPath}/tag-list">Add/Update tags</a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.menu-element').click(function(){
  		$('.menu a').filter(function() {
            $('.nav').css('').removeClass('menu-top-active');
            return this.href == window.location;
        }).parent().addClass('menu-top-active');
	});
</script>