<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<html>
<head>
	<title></title>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/layout.css">
    <script type="text/javascript"
          src="${pageContext.request.contextPath}/resources/script/jquery-2.2.3.min.js"></script>
</head>
<body>
    <div class="page">
        <tiles:insertAttribute name="header"/>
        <tiles:insertAttribute name="menu"/>
        <tiles:insertAttribute name="content"/>
        <tiles:insertAttribute name="footer" />
    </div>

</body>
</html>

