<%@ page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div class="header">
	<h1 class="header-title">News Portal - Administration</h1>

	<div class="header-menu">
		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<div class="header-logout">
				Hello, ${pageContext.request.userPrincipal.name}
				|
				<a href="">

				</a>
				<form action="logout" method="POST" style="display: inline-block">
					<input type="submit"
						   value="LogOut">

					<input type="hidden"
						   name="${_csrf.parameterName}"
						   value="${_csrf.token}" />
				</form>

			</div>
		</c:if>

		<!--<div class="header-language">
			<a href="?lang=en">  </a>
			|
			<a href="?lang=ru">  </a>
		</div>-->
	</div>
</div>



