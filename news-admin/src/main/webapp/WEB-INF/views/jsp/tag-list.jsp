<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<html>
<head>
	<title></title>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/taglist.css">
</head>

<body>
	<div class="content">
		<div class="taglist-block">
            <c:forEach items="${tagList}" var="currentTag">
                <div class="tag-box" align="left">
                	<div class="tag-title"> Tag: </div>
					<form:form action="${pageContext.request.contextPath}/update-tag" method="POST" modelAttribute="tag"
							   id="update-tag${currentTag.id}" cssClass="tag-name">

						<c:if test="${updatedTagId eq currentTag.id}">
							<form:errors path="name" cssClass="error-input"/>
						</c:if>

						<form:input path="name" value="${currentTag.name}" disabled="true"
							id="tagName${currentTag.id}"/>

						<form:hidden path="id" value="${currentTag.id}"/>

						<a id="update${currentTag.id}" onclick="updateTag(id)" class="tag-update">Update</a>
					</form:form>

					<a id="edit${currentTag.id}" class="tag-edit" onclick="showEditOptions(id)">Edit</a>

					<form action="${pageContext.request.contextPath}/delete-tag" method="POST" id="delete-tag${currentTag.id}" class="tag-name">
						<input id="deletingTagId${currentTag.id}" name="tagId" type="hidden" value="${currentTag.id}"/>

						<input type="hidden" name="${_csrf.parameterName}"
							   value="${_csrf.token}" />

						<a id="delete${currentTag.id}" onclick="deleteTag(id)" class="tag-delete">Delete</a>
					</form>

					<a id="cancel${currentTag.id}" class="tag-cancel" onclick="hideEditOptions(id)">Cancel</a>
                </div>
            </c:forEach>

			<div class="add-tag-box" align="left">
				<div class="tag-title"> Add Tag: </div>
				<form:form action="${pageContext.request.contextPath}/save-tag" method="POST" modelAttribute="newTag"
						   id="save-tag" cssClass="tag-name">

					<form:errors path="name" cssClass="error-input"/>
					<form:input path="name" maxlength="25"/>

					<a class="tag-save" onclick="saveNewTag()">Save</a>
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>

<script>

	function showEditOptions(editTagId) {
		var tagId = editTagId.split("edit")[1];

		$('#edit' + tagId).css('visibility', 'hidden');
		$('#update' + tagId).css('visibility', 'visible');
		$('#delete' + tagId).css('visibility', 'visible');
		$('#cancel' + tagId).css('visibility', 'visible');
		$('#tagName' + tagId).prop( "disabled", false );
	}

	function hideEditOptions(editOptionTagId) {
		var tagId = editOptionTagId.match(/[0-9]+$/)[0];

		$('#edit' + tagId).css('visibility', 'visible');
		$('#update' + tagId).css('visibility', 'hidden');
		$('#delete' + tagId).css('visibility', 'hidden');
		$('#cancel' + tagId).css('visibility', 'hidden');
		$('#tagName' + tagId).prop( "disabled", true );
	}

	function updateTag(updateOptionTagId) {
		var tagId = updateOptionTagId.match(/[0-9]+$/)[0];

		$('#update-tag' + tagId).submit();

		hideEditOptions(updateOptionTagId);
	}

	function deleteTag(deleteOptionTagId) {
		var tagId = deleteOptionTagId.match(/[0-9]+$/)[0];

		$('#delete-tag' + tagId).submit();

		hideEditOptions(deleteOptionTagId);
	}

	function saveNewTag() {
		$('#save-tag').submit();
	}


</script>


