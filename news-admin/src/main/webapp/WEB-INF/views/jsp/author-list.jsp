<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/authorlist.css">
</head>
<body>
<div class="content">
    <div class="authorlist-block">
        <c:forEach items="${authorList}" var="currentAuthor">
            <div class="author-box" align="left">
                <div class="author-title"> Author: </div>


                <form:form action="${pageContext.request.contextPath}/update-author" method="POST" modelAttribute="author"
                           id="update-author${currentAuthor.id}" cssClass="author-name">
                    <c:if test="${updatedAuthorId eq currentAuthor.id}">
                        <form:errors path="name" cssClass="error-input"/>
                    </c:if>
                    <form:input path="name" value="${currentAuthor.name}" id="authorName${currentAuthor.id}"
                                disabled="true" maxlength="50"/>

                    <form:hidden path="id" value="${currentAuthor.id}"/>

                    <a id="update${currentAuthor.id}" onclick="updateAuthor(id)" class="author-update">Update</a>
                </form:form>

                <a id="edit${currentAuthor.id}" class="author-edit" onclick="showEditOptions(id)">Edit</a>

                <form:form action="${pageContext.request.contextPath}/expire-author" method="POST" modelAttribute="author"
                           id="expire-author${currentAuthor.id}" cssClass="author-name">
                    <form:hidden path="id" value="${currentAuthor.id}"/>

                    <a id="expire${currentAuthor.id}" onclick="expireAuthor(id)" class="author-expire">Expire</a>
                </form:form>

                <a id="cancel${currentAuthor.id}" class="author-cancel" onclick="hideEditOptions(id)">Cancel</a>
            </div>
        </c:forEach>

        <div class="add-author-box" align="left">
            <div class="author-title"> Add Author: </div>

            <form:form action="${pageContext.request.contextPath}/save-author" method="POST" modelAttribute="newAuthor"
                       id="save-author" cssClass="author-name">


                <form:errors path="name" cssClass="error-input"/>
                <form:input path="name" maxlength="50"/>

                <a class="author-save" onclick="saveNewAuthor()">Save</a>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>

<script>

	function showEditOptions(editAuthorId) {
		var authorId = editAuthorId.split("edit")[1];

		$('#edit' + authorId).css('visibility', 'hidden');
		$('#update' + authorId).css('visibility', 'visible');
		$('#expire' + authorId).css('visibility', 'visible');
		$('#cancel' + authorId).css('visibility', 'visible');
		$('#authorName' + authorId).prop( "disabled", false );
	}

	function hideEditOptions(editOptionAuthorId) {
		var authorId = editOptionAuthorId.match(/[0-9]+$/)[0];

		$('#edit' + authorId).css('visibility', 'visible');
		$('#update' + authorId).css('visibility', 'hidden');
		$('#expire' + authorId).css('visibility', 'hidden');
		$('#cancel' + authorId).css('visibility', 'hidden');
		$('#authorName' + authorId).prop( "disabled", true );
	}

	function updateAuthor(updateOptionAuthorId) {
		var authorId = updateOptionAuthorId.match(/[0-9]+$/)[0];

		$('#update-author' + authorId).submit();

		hideEditOptions(updateOptionAuthorId);
	}

	function expireAuthor(expireOptionAuthorId) {
		var authorId = expireOptionAuthorId.match(/[0-9]+$/)[0];

		$('#expire-author' + authorId).submit();

		hideEditOptions(expireOptionAuthorId);
	}

	function saveNewAuthor() {
		$('#save-author').submit();
	}

</script>


