<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page session="true"%>

<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/resources/css/multiple-select/multiple-select.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/multiple-select/multiple-select.js"></script>
<link rel="stylesheet" type="text/css"
  href="${pageContext.request.contextPath}/resources/css/newslist.css">

<div class="content">
    <div class="search-block" align="left">
        <form action="${pageContext.request.contextPath}/filter-news" method="POST" class="filter-form">
            <select id="author-list" name="authorId" class="select-author" data-placeholder="Select author">
                <option value="null" disabled selected>Select author</option>
                <c:forEach items="${authorList}" var="currentAuthor">
                    <c:if test="${currentAuthor.expiredDate == null}">
                        <option value="${currentAuthor.id}">${currentAuthor.name}</option>
                    </c:if>
                </c:forEach>
            </select>
            <select id="tag-list" name="tagId" multiple="multiple" class="select-tag">
                <c:forEach items="${tagList}" var="currentTag">
                    <option value="${currentTag.id}">${currentTag.name}</option>
                </c:forEach>
            </select>

            <input type="submit" value="Filter" class="filter-button"/>

            <input type="hidden" name="${_csrf.parameterName}"
                   value="${_csrf.token}" />
        </form>
        <form action="${pageContext.request.contextPath}/reset-filter" method="POST" class="reset-form">
            <input type="submit" value="Reset" class="reset-button "/>

            <input type="hidden" name="${_csrf.parameterName}"
                   value="${_csrf.token}" />
        </form >
    </div>


    <div class="newslist-block">
        <c:if test="${fn:length(fullNewsList) == 0 }">
            <p>
                <strong>Sorry, but the content you requested could not be found.</strong>
            </p>
        </c:if>
        <form action="${pageContext.request.contextPath}/delete-news" name="deleteNews" method="POST">
            <c:forEach items="${fullNewsList}" var="fullNews">
                <div class="news-box">
                    <div align="left">
                        <div class="news-title">
                            <a href="${pageContext.request.contextPath}/news/${fullNews.news.id}">${fullNews.news.title}</a>
                        </div>
                        <div class="news-author">  (by ${fullNews.author.name})</div>
                        <div class="news-date">
                            <fmt:formatDate pattern="dd/MM/yyyy" value="${fullNews.news.creationDate}" />
                        </div>
                    </div>
                    <p>${fullNews.news.shortText}</p>
                    <div align="right">
                        <c:forEach items="${fullNews.tags}" var="currentTag">
                            <div class="news-tag">${currentTag.name}, </div>
                        </c:forEach>
                        <div class="news-comments">Comments(${fn:length(fullNews.comments)})</div>
                        <div class="news-edit">
                            <a href="${pageContext.request.contextPath}/edit-news?newsId=${fullNews.news.id}">Edit</a>
                        </div>
                        <div class="news-delete">
                            <input type="checkbox" name="deleteNews" id="delete-news"
                                value="${fullNews.news.id}" onClick="makeDeleteIsActive()"/>
                        </div>
                    </div>
                </div>
            </c:forEach>

            <input type="hidden" name="${_csrf.parameterName}"
                   value="${_csrf.token}" />

            <c:if test="${fn:length(fullNewsList) != 0 }">
                <div class="button-delete">
                    <input type="submit" value="Delete" id="delete-button" disabled/>
                </div>
            </c:if>
        </form>
    </div>

    <div class="pagination-block">
        <div class="pagination">
            <c:forEach begin="1" end="${pagesNumber}" var="numberOfPage">
                <c:choose>
                    <c:when test="${currentPage == numberOfPage}">
                        <span class="active-page">${numberOfPage}</span>
                    </c:when>
                    <c:otherwise>
                        <a class="inactive-page" href="${pageContext.request.contextPath}/news-list/${numberOfPage}">${numberOfPage}</a>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </div>
    </div>

</div>

<script>
    function makeDeleteIsActive() {
        var activeCheckboxes = $('#delete-news:checked');
        if (activeCheckboxes.length != 0) {
            $('#delete-button').prop( "disabled", false);
        } else {
            $('#delete-button').prop( "disabled", true);
        }
    }

     $('#tag-list').multipleSelect({
            placeholder: 'Select tags'
     });
</script>