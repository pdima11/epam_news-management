<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="login-form">

	<c:if test="${not empty error}">
		<div class="message">ERROR</div>
	</c:if>
	<c:if test="${not empty logout}">
		<div class="message">logout success</div>
	</c:if>

    <form name="login_form" action="j_spring_security_check"
          method="POST" id="loginForm">

        <input class="login-field" type="text"
               placeholder="Login" name="login"/>

        <input class="login-field" type="password"
               placeholder="Password" name="password" />

        <input class="login-field" type="submit" name="submit" />

        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}" />
    </form>
</div>
