package by.epam.task2.newsadmin.controller;

import by.epam.task1.domain.Author;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;


@Controller
public class AuthorController {
    private static final String AUTHOR_LIST_VIEW = "author-list";
    private static final String REDIRECT_PAGE = "redirect:/author-list";

    private static final String AUTHOR_ATTRIBUTE = "author";
    private static final String NEW_AUTHOR_ATTRIBUTE = "newAuthor";
    private static final String AUTHOR_LIST_ATTRIBUTE = "authorList";

    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = "/author-list", method = RequestMethod.GET)
    public String showTagList(Model model) throws ServiceException{
        List<Author> authorList = authorService.readAuthorList();

        model.addAttribute(AUTHOR_LIST_ATTRIBUTE, authorList);

        if (!model.containsAttribute(AUTHOR_ATTRIBUTE)) {
            model.addAttribute(AUTHOR_ATTRIBUTE, new Author());
        }
        if (!model.containsAttribute(NEW_AUTHOR_ATTRIBUTE)) {
            model.addAttribute(NEW_AUTHOR_ATTRIBUTE, new Author());
        }

        return AUTHOR_LIST_VIEW;
    }

    @RequestMapping(value = "/save-author", method = RequestMethod.POST)
    public String saveAuthor(@Valid @ModelAttribute(NEW_AUTHOR_ATTRIBUTE) Author author,
                             BindingResult result, RedirectAttributes redirectAttr) throws ServiceException{
        if (result.hasErrors()) {
            redirectAttr.addFlashAttribute(NEW_AUTHOR_ATTRIBUTE, author);
            redirectAttr.addFlashAttribute("org.springframework.validation.BindingResult." + NEW_AUTHOR_ATTRIBUTE, result);

            return REDIRECT_PAGE;
        }

        authorService.create(author);

        return REDIRECT_PAGE;
    }

    @RequestMapping(value = "/update-author", method = RequestMethod.POST)
    public String updateAuthor(@Valid @ModelAttribute(AUTHOR_ATTRIBUTE) Author author,
                               BindingResult result, RedirectAttributes redirectAttr) throws ServiceException{
        if (result.hasErrors()) {
            redirectAttr.addFlashAttribute("org.springframework.validation.BindingResult." + AUTHOR_ATTRIBUTE, result);
            redirectAttr.addFlashAttribute(AUTHOR_ATTRIBUTE, author);
            redirectAttr.addFlashAttribute("updatedAuthorId", author.getId());

            return REDIRECT_PAGE;
        }

        authorService.update(author);

        return REDIRECT_PAGE;
    }

    @RequestMapping(value = "/expire-author", method = RequestMethod.POST)
    public String expireAuthor(@ModelAttribute(AUTHOR_ATTRIBUTE) Author author) throws ServiceException{
        authorService.makeExpired(author);

        return REDIRECT_PAGE;
    }


}
