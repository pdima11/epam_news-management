package by.epam.task2.newsadmin.controller;

import by.epam.task1.domain.*;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.AuthorService;
import by.epam.task1.service.CommentService;
import by.epam.task1.service.FullNewsService;
import by.epam.task1.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class NewsController {
    private static final String NEWS_VIEW = "news";
    private static final String NEWS_LIST_VIEW = "news-list";
    private static final String EDIT_NEWS_VIEW = "edit-news";
    private static final String ADD_NEWS_VIEW = "add-news";

    private static final String REDIRECT_ON_NEWS_VIEW = "redirect:/news/";
    private static final String REDIRECT_ON_NEWS_LIST_VIEW = "redirect:/news-list";
    private static final String INCORRECT_REDIRECT_PAGE_FOR_ADDING_NEWS = "redirect:/add-news";
    private static final String INCORRECT_REDIRECT_PAGE_FOR_UPDATING_NEWS = "redirect:/update-news";

    private static final String NEWS_ATTRIBUTE = "news";
    private static final String NEWS_AUTHOR_ATTRIBUTE = "newsAuthor";
    private static final String NEWS_TAG_LIST_ATTRIBUTE = "newsTagList";
    private static final String FULL_NEWS_LIST_ATTRIBUTE = "fullNewsList";
    private static final String TAG_LIST_ATTRIBUTE = "tagList";
    private static final String AUTHOR_LIST_ATTRIBUTE = "authorList";
    private static final String CURRENT_PAGE_ATTRIBUTE = "currentPage";
    private static final String PAGES_NUMBER_ATTRIBUTE = "pagesNumber";
    private static final String SEARCH_CRITERIA_ATTRIBUTE = "searchCriteria";
    private static final String FULL_NEWS_ATTRIBUTE = "fullNews";
    private static final String NEW_COMMENT_ATTRIBUTE = "newComment";

    @Autowired
    private FullNewsService fullNewsService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @RequestMapping(value = "/news-list/{currentPage:\\d}", method = RequestMethod.GET)
    public String showNewsList(@PathVariable Long currentPage,
                               HttpSession session, Model model) throws ServiceException{
        SearchCriteriaTO searchCriteria = (SearchCriteriaTO)session.getAttribute(SEARCH_CRITERIA_ATTRIBUTE);
        List<FullNewsTO> fullNewsList;
        List<Author> authorList = authorService.readAuthorList();
        List<Tag> tagList = tagService.readTagList();
        Long pagesNumber;

        if (searchCriteria == null) {
            pagesNumber = fullNewsService.getPagesNumber();
            fullNewsList = fullNewsService.readNewsList(currentPage);
        } else {
            pagesNumber = 1L;
            fullNewsList = fullNewsService.search(currentPage, searchCriteria);
        }

        model.addAttribute(FULL_NEWS_LIST_ATTRIBUTE, fullNewsList);
        model.addAttribute(TAG_LIST_ATTRIBUTE, tagList);
        model.addAttribute(AUTHOR_LIST_ATTRIBUTE, authorList);
        model.addAttribute(CURRENT_PAGE_ATTRIBUTE, currentPage);
        model.addAttribute(PAGES_NUMBER_ATTRIBUTE, pagesNumber);

        return NEWS_LIST_VIEW;
    }

    @RequestMapping(value = {"/", "/news-list"}, method = {RequestMethod.GET})
    public String showHomePage(HttpSession session, Model model) throws ServiceException{
        return showNewsList(1L, session, model);
    }

    @RequestMapping(value = "/news/{newsId:\\d+}", method = RequestMethod.GET)
    public String showNews(@PathVariable Long newsId, Model model) throws ServiceException{
        FullNewsTO fullNews = fullNewsService.read(newsId);

        model.addAttribute(FULL_NEWS_ATTRIBUTE, fullNews);
        if (!model.containsAttribute(NEW_COMMENT_ATTRIBUTE)) {
            model.addAttribute(NEW_COMMENT_ATTRIBUTE, new Comment());
        }

        return NEWS_VIEW;
    }

    @RequestMapping(value = "delete-news", method = RequestMethod.POST)
    public String deleteNews(@RequestParam(value = "deleteNews",
            required = false) Long[] newsIdList) throws ServiceException{

        for (Long newsId: newsIdList) {
            fullNewsService.delete(newsId);
        }

        return REDIRECT_ON_NEWS_LIST_VIEW;
    }


    @RequestMapping(value = "filter-news", method = RequestMethod.POST)
    public String filterNews(@RequestParam(required = false) Long authorId,
                             @RequestParam(required = false) Long[] tagId, HttpSession session) {

        List<Long> tagIdList = null;
        if (tagId != null) {
            tagIdList = new ArrayList<Long>(Arrays.asList(tagId));
        }

        SearchCriteriaTO searchCriteria = new SearchCriteriaTO();
        searchCriteria.setAuthorId(authorId);
        searchCriteria.setTagIdList(tagIdList);

        session.setAttribute(SEARCH_CRITERIA_ATTRIBUTE, searchCriteria);

        return REDIRECT_ON_NEWS_LIST_VIEW;
    }


    @RequestMapping(value = "reset-filter", method = RequestMethod.POST)
    public String resetFilter(HttpSession session) {
        session.removeAttribute(SEARCH_CRITERIA_ATTRIBUTE);
        return REDIRECT_ON_NEWS_LIST_VIEW;
    }


    @RequestMapping(value = "/add-comment", method = RequestMethod.POST)
    public String addComment(@RequestParam Long newsId,
                             @Valid @ModelAttribute(NEW_COMMENT_ATTRIBUTE) Comment comment,
                             BindingResult result, RedirectAttributes redirectAttr) throws ServiceException {
        if (result.hasErrors()) {
            redirectAttr.addFlashAttribute("org.springframework.validation.BindingResult." + NEW_COMMENT_ATTRIBUTE, result);
            redirectAttr.addFlashAttribute(NEW_COMMENT_ATTRIBUTE, comment);

            return REDIRECT_ON_NEWS_VIEW + newsId;
        }

        commentService.create(comment);

        return REDIRECT_ON_NEWS_VIEW + newsId;
    }


    @RequestMapping(value = "/delete-comment", method = RequestMethod.POST)
    public String deleteComment(@RequestParam Long newsId,
                                @RequestParam Long commentId) throws ServiceException{
        commentService.delete(commentId);

        return REDIRECT_ON_NEWS_VIEW + newsId;
    }


    @RequestMapping(value = "/edit-news", method = RequestMethod.GET)
    public String editNews(@RequestParam Long newsId, Model model) throws ServiceException{
        FullNewsTO fullNews = fullNewsService.read(newsId);
        List<Tag> tagList = tagService.readTagList();
        List<Author> authorList = authorService.readAuthorList();

        model.addAttribute(NEWS_AUTHOR_ATTRIBUTE, fullNews.getAuthor());
        model.addAttribute(NEWS_TAG_LIST_ATTRIBUTE, fullNews.getTags());
        model.addAttribute(AUTHOR_LIST_ATTRIBUTE, authorList);
        model.addAttribute(TAG_LIST_ATTRIBUTE, tagList);
        if (!model.containsAttribute(NEWS_ATTRIBUTE)) {
            model.addAttribute(NEWS_ATTRIBUTE, fullNews.getNews());
        }

        return EDIT_NEWS_VIEW;
    }

    @RequestMapping(value = "/add-news", method = RequestMethod.GET)
    public String showAddNewsPage(Model model) throws ServiceException {
        List<Author> authorList = authorService.readAuthorList();
        List<Tag> tagList = tagService.readTagList();

        model.addAttribute(AUTHOR_LIST_ATTRIBUTE, authorList);
        model.addAttribute(TAG_LIST_ATTRIBUTE, tagList);
        if (!model.containsAttribute(NEWS_ATTRIBUTE)) {
            model.addAttribute(NEWS_ATTRIBUTE, new News());
        }

        return ADD_NEWS_VIEW;
    }


    @RequestMapping(value = "/add-new-news", method = RequestMethod.POST)
    public String addNewNews(@RequestParam Long authorId,
                             @RequestParam(required = false) Long[] tagIdList,
                             @Valid @ModelAttribute(NEWS_ATTRIBUTE) News news,
                             BindingResult result, RedirectAttributes redirectAttr) throws ServiceException{
        if (result.hasErrors()) {
            redirectAttr.addFlashAttribute("org.springframework.validation.BindingResult." + NEWS_ATTRIBUTE, result);
            redirectAttr.addFlashAttribute(NEWS_ATTRIBUTE, news);

            return INCORRECT_REDIRECT_PAGE_FOR_ADDING_NEWS;
        }

        FullNewsTO fullNews = new FullNewsTO();

        setFullNewsInfo(fullNews, news, authorId, tagIdList);

        Long newsId  = fullNewsService.create(fullNews);

        return REDIRECT_ON_NEWS_VIEW + newsId;
    }



    @RequestMapping(value = "/update-news", method = RequestMethod.POST)
    public String updateNews(@RequestParam Long authorId,
                             @RequestParam(required = false) Long[] tagIdList,
                             @Valid @ModelAttribute(NEWS_ATTRIBUTE) News news,
                             BindingResult result, RedirectAttributes redirectAttr) throws ServiceException{

        if (result.hasErrors()) {
            redirectAttr.addFlashAttribute("org.springframework.validation.BindingResult." + NEWS_ATTRIBUTE, result);
            redirectAttr.addAttribute(NEWS_ATTRIBUTE, news);

            return INCORRECT_REDIRECT_PAGE_FOR_UPDATING_NEWS;
        }
        FullNewsTO fullNews = new FullNewsTO();

        setFullNewsInfo(fullNews, news, authorId, tagIdList);

        fullNewsService.update(fullNews);

        return REDIRECT_ON_NEWS_VIEW + news.getId();
    }


    private void setFullNewsInfo(FullNewsTO fullNews, News news,
                                 Long authorId, Long[] tagIdList) {
        Author author = new Author();
        author.setId(authorId);

        List<Tag> tagList = new ArrayList<Tag>();
        if (tagIdList != null) {
            for (Long tagId: tagIdList) {
                Tag tag = new Tag();

                tag.setId(tagId);
                tagList.add(tag);
            }
        }

        fullNews.setNews(news);
        fullNews.setAuthor(author);
        fullNews.setTags(tagList);
    }
}
