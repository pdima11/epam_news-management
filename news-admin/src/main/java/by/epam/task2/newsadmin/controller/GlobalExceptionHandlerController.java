package by.epam.task2.newsadmin.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class GlobalExceptionHandlerController {
    private static final Logger logger = LogManager.getLogger();
    private static final String ERROR_VIEW = "error";

    @ExceptionHandler(Exception.class)
    public String handleError(Exception e) {
        logger.error(e);
        return ERROR_VIEW;
    }
}
