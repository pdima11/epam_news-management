package by.epam.task2.newsadmin.controller;

import by.epam.task1.domain.Tag;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.TagService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;


@Controller
public class TagController {
    private static final String TAG_LIST_VIEW = "tag-list";
    private static final String REDIRECT_PAGE = "redirect:/tag-list";

    private static final String TAG_ATTRIBUTE = "tag";
    private static final String NEW_TAG_ATTRIBUTE = "newTag";
    private static final String TAG_LIST_ATTRIBUTE = "tagList";

    @Autowired
    private TagService tagService;

    @RequestMapping(value = "/tag-list", method = RequestMethod.GET)
    public String showTagList(Model model) throws ServiceException{
        List<Tag> tagList = tagService.readTagList();

        model.addAttribute(TAG_LIST_ATTRIBUTE, tagList);
        if (!model.containsAttribute(TAG_ATTRIBUTE)) {
            model.addAttribute(TAG_ATTRIBUTE, new Tag());
        }
        if (!model.containsAttribute(NEW_TAG_ATTRIBUTE)) {
            model.addAttribute(NEW_TAG_ATTRIBUTE, new Tag());
        }

        return TAG_LIST_VIEW;
    }

    @RequestMapping(value = "/save-tag", method = RequestMethod.POST)
    public String saveTag(@Valid @ModelAttribute(NEW_TAG_ATTRIBUTE) Tag tag,
                          BindingResult result, RedirectAttributes redirectAttr) throws ServiceException{
        if (result.hasErrors()) {
            redirectAttr.addFlashAttribute("org.springframework.validation.BindingResult." + NEW_TAG_ATTRIBUTE, result);
            redirectAttr.addFlashAttribute(NEW_TAG_ATTRIBUTE, tag);

            return REDIRECT_PAGE;
        }

        tagService.create(tag);

        return REDIRECT_PAGE;
    }

    @RequestMapping(value = "/update-tag", method = RequestMethod.POST)
    public String updateTag(@Valid @ModelAttribute(TAG_ATTRIBUTE) Tag tag,
                            BindingResult result, RedirectAttributes redirectAttr) throws ServiceException{
        if (result.hasErrors()) {
            redirectAttr.addFlashAttribute(TAG_ATTRIBUTE, tag);
            redirectAttr.addFlashAttribute("org.springframework.validation.BindingResult." + TAG_ATTRIBUTE, result);
            redirectAttr.addFlashAttribute("updatedTagId", tag.getId());

            return REDIRECT_PAGE;
        }

        tagService.update(tag);

        return REDIRECT_PAGE;
    }

    @RequestMapping(value = "/delete-tag", method = RequestMethod.POST)
    public String deleteTag(@RequestParam Long tagId) throws ServiceException{
        tagService.delete(tagId);

        return REDIRECT_PAGE;
    }
}
