package by.epam.task2.newsadmin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {
    private static final String LOG_IN_VIEW = "signin";

    private static final String ERROR = "error";
    private static final String LOGOUT = "logout";
    private static final String ERROR_MESSAGE = "error";
    private static final String LOGOUT_MESSAGE = "logout";

    @RequestMapping("/signin")
    public String login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout,
            Model model) {

        if (error != null) {
            model.addAttribute(ERROR, ERROR_MESSAGE);
        }
        if (logout != null) {
            model.addAttribute(LOGOUT, LOGOUT_MESSAGE);
        }

        return LOG_IN_VIEW;
    }

}
