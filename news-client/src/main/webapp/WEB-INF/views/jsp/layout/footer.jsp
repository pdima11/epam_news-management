<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/resources/css/layout.css">

<div class="footer">
    © 1993-2016. EPAM Systems. All Rights Reserved.
</div>
