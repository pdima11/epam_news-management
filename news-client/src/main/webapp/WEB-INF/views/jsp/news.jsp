<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
    <title>${fullNews.news.title}</title>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/news.css">>
</head>
<body>

    <jsp:include page="layout/header.jsp"/>

    <div class="content">

        <div>
            <a href="${pageContext.request.contextPath}/news-management/news-list">BACK</a>
        </div>

        <div class="news-block">
            <div align="left">
                <div class="news-title">
                    ${fullNews.news.title}
                </div>
                <div class="news-author">(by ${fullNews.author.name})</div>
                <div class="news-date">
                    <fmt:formatDate pattern="dd/MM/yyyy" value="${fullNews.news.creationDate}" />
                </div>
            </div>
            <p>${fullNews.news.fullText}</p>
        </div>

        <div class="comments-block">
            <c:forEach items="${fullNews.comments}" var="comment">
                <div class="comment-box">
                    <div class="comment-date">
                        <fmt:formatDate pattern="dd/MM/yyyy" value="${comment.creationDate}" />
                    </div>
                    <br>
                    <p class="comment-message">
                            ${comment.text}
                    </p>
                </div>
            </c:forEach>

            <div>
                <c:if test="${commentTextError == true}">
                    ${errorMessage}
                </c:if>
            </div>

            <div class="add-comment">
                <form action="${pageContext.request.contextPath}/news-management/add-comment" method="POST" modelAttribute="newComment">
                    <textarea path="text" maxlength="500" name="commentText"></textarea>
                    <input type="hidden" name="newsId" value="${fullNews.news.id}"/>

                    <input type="submit" value="Post Comment"/>
                </form>
            </div>
        </div>
    </div>

    <jsp:include page="layout/footer.jsp"/>

</body>
</html>