<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>404</title>
</head>
<body>
<div>
    <h1>Oops!</h1>
    <h2>404 Page Not Found</h2>
    <div>
        Requested page not found!
    </div>
    <div>
        <a href="${pageContext.request.contextPath}/news-management/news-list">main</a>
    </div>
</div>
</body>
</html>