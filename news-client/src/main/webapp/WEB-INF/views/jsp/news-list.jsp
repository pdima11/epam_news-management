<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/multiple-select/multiple-select.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/newslist.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/css/layout.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/multiple-select/multiple-select.js"></script>
</head>
<body>
    <jsp:include page="layout/header.jsp"/>
    <div class="content">
        <div class="search-block" align="left">
            <form action="${pageContext.request.contextPath}/news-management/filter-news" method="POST" class="filter-form">
                <select id="author-list" name="authorId" class="select-author">
                    <option value="null" disabled selected>Select author</option>
                    <c:forEach items="${authorList}" var="currentAuthor">
                        <c:if test="${currentAuthor.expiredDate == null}">
                            <option value="${currentAuthor.id}">${currentAuthor.name}</option>
                        </c:if>
                    </c:forEach>
                </select>
                <select id="tag-list" name="tagId" multiple="multiple" class="select-tag">
                    <c:forEach items="${tagList}" var="currentTag">
                        <option value="${currentTag.id}">${currentTag.name}</option>
                    </c:forEach>
                </select>
                <input type="submit" value="Filter" class="filter-button"/>
            </form>
            <form action="${pageContext.request.contextPath}/news-management/reset-filter" method="POST" class="reset-form">
                <input type="submit" value="Reset" class="reset-button "/>
            </form >
        </div>


        <div class="newslist-block">
            <c:if test="${fn:length(fullNewsList) == 0 }">
                <p>
                    <strong>Sorry, but the content you requested could not be found.</strong>
                </p>
            </c:if>

            <c:forEach items="${fullNewsList}" var="fullNews">
                <div class="news-box">
                    <div align="left">
                        <div class="news-title">
                            <a href="${pageContext.request.contextPath}/news-management/news/${fullNews.news.id}/">${fullNews.news.title}</a>
                        </div>
                        <div class="news-author">  (by ${fullNews.author.name})</div>
                        <div class="news-date">
                            <fmt:formatDate pattern="dd/MM/yyyy" value="${fullNews.news.creationDate}" />
                        </div>
                    </div>
                    <p>${fullNews.news.shortText}</p>
                    <div align="right">
                        <c:forEach items="${fullNews.tags}" var="currentTag">
                            <div class="news-tag">${currentTag.name}, </div>
                        </c:forEach>
                        <div class="news-comments">Comments(${fn:length(fullNews.comments)})</div>
                    </div>
                </div>
            </c:forEach>
        </div>

        <div class="pagination-block">
            <div class="pagination">
                <c:forEach begin="1" end="${pagesNumber}" var="numberOfPage">
                    <c:choose>
                        <c:when test="${currentPage == numberOfPage}">
                            <span class="active-page">${numberOfPage}</span>
                        </c:when>
                        <c:otherwise>
                            <a class="inactive-page" href="${pageContext.request.contextPath}/news-management/news-list/${numberOfPage}/">${numberOfPage}</a>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </div>
        </div>
    </div>
    <jsp:include page="layout/footer.jsp"/>
</body>
</html>

<script>
    $('#tag-list').multipleSelect({
        placeholder: 'Select tags'
    });
</script>
