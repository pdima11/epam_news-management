package by.epam.task2.newsclient.controller.command;

import by.epam.task1.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;


public interface Command {
    String execute(HttpServletRequest request) throws ServiceException;
}
