package by.epam.task2.newsclient.controller.command.impl;

import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.exception.ServiceException;
import by.epam.task2.newsclient.controller.command.Command;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Component
public class FilterNewsCommand implements Command {
    private static final String REDIRECT_VIEW = "/news-management/news-list";
    private static final String AUTHOR_ID_ATTRIBUTE = "authorId";
    private static final String TAG_ID_LIST_ATTRIBUTE = "tagId";
    private static final String SEARCH_CRITERIA_ATTRIBUTE = "searchCriteria";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String checkedAuthorId = request.getParameter(AUTHOR_ID_ATTRIBUTE);
        String[] checkedTagId = request.getParameterValues(TAG_ID_LIST_ATTRIBUTE);

        Long authorId = null;
        if (checkedAuthorId != null) {
            authorId = Long.valueOf(checkedAuthorId);
        }

        List<Long> tagIdList = null;
        if (checkedTagId != null) {
            tagIdList = new ArrayList<>();
            for (String tagId: checkedTagId) {
                tagIdList.add(Long.valueOf(tagId));
            }
        }

        SearchCriteriaTO searchCriteria = new SearchCriteriaTO();
        searchCriteria.setAuthorId(authorId);
        searchCriteria.setTagIdList(tagIdList);

        request.getSession().setAttribute(SEARCH_CRITERIA_ATTRIBUTE, searchCriteria);

        return REDIRECT_VIEW;
    }
}
