package by.epam.task2.newsclient.controller;

import by.epam.task2.newsclient.controller.command.Command;
import by.epam.task2.newsclient.controller.command.CommandHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class ClientServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger();

    private static final String ERROR_PAGE = "/WEB-INF/views/jsp/errors/error.jsp";

    private static final String FILTER_NEWS_COMMAND = "filterNewsCommand";
    private static final String RESET_FILTER_COMMAND = "resetFilterCommand";
    private static final String ADD_COMMENT_COMMAND = "addCommentCommand";
    private static final String DEFAULT_COMMAND = "newsListCommand";


    private static WebApplicationContext context;

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request,
                                HttpServletResponse response) throws ServletException, IOException{
        String path = request.getPathInfo();
        String commandName;

        if (path == null) {
            commandName = DEFAULT_COMMAND;
        } else {
            commandName = CommandHelper.getCommand(path);
        }

        String forwardPage = null;
        try {
            Command command = (Command) context.getBean(commandName);
            forwardPage = command.execute(request);
        } catch (Exception e) {
            logger.error("", e);
            request.getRequestDispatcher(ERROR_PAGE).forward(request, response);
        }

        if (FILTER_NEWS_COMMAND.equals(commandName) ||
                RESET_FILTER_COMMAND.equals(commandName) ||
                ADD_COMMENT_COMMAND.equals(commandName)) {
            response.sendRedirect(forwardPage);
        } else {
            request.getRequestDispatcher(forwardPage).forward(request, response);
        }
    }




}
