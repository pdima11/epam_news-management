package by.epam.task2.newsclient.controller.command.impl;

import by.epam.task1.domain.FullNewsTO;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.FullNewsService;
import by.epam.task2.newsclient.controller.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class NewsCommand implements Command {
    private static final String NEWS_VIEW = "/WEB-INF/views/jsp/news.jsp";

    private static final String FULL_NEWS_ATTRIBUTE = "fullNews";

    @Autowired
    private FullNewsService fullNewsService;


    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String path = request.getPathInfo();

        Long newsId = getNewsId(path);

        FullNewsTO fullNewsTO = fullNewsService.read(newsId);

        request.setAttribute(FULL_NEWS_ATTRIBUTE, fullNewsTO);

        return NEWS_VIEW;
    }

    private Long getNewsId(String path) {
        String[] parameters = path.split("/");

        return Long.valueOf(parameters[parameters.length - 1]);
    }


}
