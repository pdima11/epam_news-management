package by.epam.task2.newsclient.controller.command.impl;

import by.epam.task1.exception.ServiceException;
import by.epam.task2.newsclient.controller.command.Command;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class ResetFilterCommand implements Command {
    private static final String REDIRECT_VIEW = "/news-management/news-list";

    private static final String SEARCH_CRITERIA_ATTRIBUTE = "searchCriteria";


    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        request.getSession().removeAttribute(SEARCH_CRITERIA_ATTRIBUTE);

        return REDIRECT_VIEW;
    }
}
