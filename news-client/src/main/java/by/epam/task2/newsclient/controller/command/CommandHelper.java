package by.epam.task2.newsclient.controller.command;

/**
 * Created by User on 25.04.2016.
 */
public class CommandHelper {

    public static String getCommand(String path) {
        path = path.replace("news-management/", "");
        String[] parameters = path.split("/");
        String[] commandParts = parameters[1].split("-");
        String commandName = commandParts[0];

        for (int i = 1; i < commandParts.length; i++) {
            commandParts[i] = commandParts[i].substring(0, 1).toUpperCase() +
                    commandParts[i].substring(1);
            commandName += commandParts[i];
        }

        return commandName + "Command";
    }

}
