package by.epam.task2.newsclient.controller.command.impl;

import by.epam.task1.domain.Comment;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.CommentService;
import by.epam.task2.newsclient.controller.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AddCommentCommand implements Command {
    private static final String REDIRECT_VIEW = "news-management/news/";

    private static final String COMMENT_TEXT_ERROR_MESSAGE = "comment size should be between 1 and 500 characters";

    private static final String ERROR_MESSAGE_ATTRIBUTE = "errorMessage";
    private static final String COMMENT_TEXT_ATTRIBUTE = "commentText";
    private static final String COMMENT_TEXT_ERROR_ATTRIBUTE = "commentTextError";
    private static final String NEWS_ID_ATTRIBUTE = "newsId";

    @Autowired
    private CommentService commentService;

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

        String commentText = request.getParameter(COMMENT_TEXT_ATTRIBUTE);
        String chosenNewsId = request.getParameter(NEWS_ID_ATTRIBUTE);

        Long newsId =  null;
        if (chosenNewsId != null ) {
            newsId = Long.valueOf(chosenNewsId);
        }

        boolean isCommentTextValidation = validateCommentTextLenght(commentText);

        if (isCommentTextValidation) {
            Comment comment = new Comment();
            comment.setNewsId(newsId);
            comment.setText(commentText);

            commentService.create(comment);

            request.setAttribute(COMMENT_TEXT_ERROR_ATTRIBUTE, false);
        } else {
            request.setAttribute(COMMENT_TEXT_ERROR_ATTRIBUTE, true);
            request.setAttribute(ERROR_MESSAGE_ATTRIBUTE, COMMENT_TEXT_ERROR_MESSAGE);
        }

        return REDIRECT_VIEW + newsId;
    }


    private boolean validateCommentTextLenght(String commentText) {
        if ((commentText.length() == 0) || (commentText.length() > 500)) {
            return false;
        }

        return true;
    }
}
