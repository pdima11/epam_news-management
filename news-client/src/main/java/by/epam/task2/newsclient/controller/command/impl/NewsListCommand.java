package by.epam.task2.newsclient.controller.command.impl;

import by.epam.task1.domain.Author;
import by.epam.task1.domain.FullNewsTO;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.AuthorService;
import by.epam.task1.service.FullNewsService;
import by.epam.task1.service.TagService;
import by.epam.task2.newsclient.controller.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component
public class NewsListCommand implements Command {
    private static final String NEWS_LIST_VIEW = "/WEB-INF/views/jsp/news-list.jsp";

    private static final String FULL_NEWS_LIST_ATTRIBUTE = "fullNewsList";
    private static final String TAG_LIST_ATTRIBUTE = "tagList";
    private static final String AUTHOR_LIST_ATTRIBUTE = "authorList";
    private static final String SEARCH_CRITERIA_ATTRIBUTE = "searchCriteria";
    private static final String CURRENT_PAGE_ATTRIBUTE = "page";
    private static final String PAGES_NUMBER_ATTRIBUTE = "pagesNumber";


    @Autowired
    private FullNewsService fullNewsService;

    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        SearchCriteriaTO searchCriteria = (SearchCriteriaTO) request.getSession().getAttribute(SEARCH_CRITERIA_ATTRIBUTE);

        String path = request.getPathInfo();

        Long currentPage = getCurrentPageNumber(path);

        List<FullNewsTO> fullNewsList;
        Long pagesNumber;

        if (searchCriteria == null) {
            pagesNumber = fullNewsService.getPagesNumber();
            fullNewsList = fullNewsService.readNewsList(currentPage);
        } else {
            pagesNumber = 1L;
            fullNewsList = fullNewsService.search(currentPage, searchCriteria);
        }

        List<Tag> tagList = tagService.readTagList();
        List<Author> authorList = authorService.readAuthorList();

        request.setAttribute(FULL_NEWS_LIST_ATTRIBUTE, fullNewsList);
        request.setAttribute(AUTHOR_LIST_ATTRIBUTE, authorList);
        request.setAttribute(TAG_LIST_ATTRIBUTE, tagList);
        request.setAttribute(PAGES_NUMBER_ATTRIBUTE, pagesNumber);
        request.setAttribute(CURRENT_PAGE_ATTRIBUTE, currentPage);

        return NEWS_LIST_VIEW;
    }

    private Long getCurrentPageNumber(String path) {
        if (path == null) {
            return 1L;
        }

        String[] parameters = path.split("/");

        if ("news-list".equals(parameters[parameters.length - 1])) {
            return 1L;
        } else {
            return Long.valueOf(parameters[parameters.length - 1]);
        }

    }
}
