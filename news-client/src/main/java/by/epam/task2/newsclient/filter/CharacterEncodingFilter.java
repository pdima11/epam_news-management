package by.epam.task2.newsclient.filter;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;

import java.io.IOException;

public class CharacterEncodingFilter implements Filter {
    private static final String ENCODING_UTF8 = "UTF-8";

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String encodingRequest = request.getCharacterEncoding();
        String encodingResponse = response.getCharacterEncoding();

        if (!(ENCODING_UTF8).equalsIgnoreCase(encodingRequest)) {
            request.setCharacterEncoding(ENCODING_UTF8);
        }
        if (!(ENCODING_UTF8).equalsIgnoreCase(encodingResponse)) {
            response.setCharacterEncoding(ENCODING_UTF8);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }


}
