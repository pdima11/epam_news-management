package by.epam.task1.service.impl;

import by.epam.task1.dao.AuthorDAO;
import by.epam.task1.domain.Author;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/spring-context.xml"})
public class AuthorServiceImplTest {

    @Mock
    private AuthorDAO authorDAO;

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Test
    public void testCreate() throws Exception {
        Long expectedAuthorId = 1L;

        Author author = new Author();
        when(authorService.create(author)).thenReturn(expectedAuthorId);

        Long authorId = authorService.create(author);
        verify(authorDAO, times(1)).create(author);

        assertEquals(authorId, expectedAuthorId);
    }

    @Test
    public void testRead() throws Exception {
        Long authorId = 1L;
        String authorName = "testAuthorName";

        Author expectedAuthor = new Author();
        expectedAuthor.setId(authorId);
        expectedAuthor.setName(authorName);

        when(authorService.read(authorId)).thenReturn(expectedAuthor);

        Author actualAuthor = authorService.read(authorId);

        verify(authorDAO, times(1)).read(authorId);
        assertEquals(expectedAuthor.getId(), actualAuthor.getId());
        assertEquals(expectedAuthor.getName(), actualAuthor.getName());
    }

    @Test
    public void testUpdate() throws Exception {
        Long authorId = 1L;
        String authorName = "updateAuthorName";

        Author author = new Author();
        author.setId(authorId);
        author.setName(authorName);

        authorService.update(author);

        verify(authorDAO, times(1)).update(author);
    }

    @Test
    public void testDelete() throws Exception {
        Long authorId = 1L;

        authorService.delete(authorId);

        verify(authorDAO, times(1)).delete(authorId);
    }

    @Test
    public void testReadAuthorListPerPage() throws Exception {
        int pageNumber = 2;
        int startPage = 12;
        int endPosition = 23;

        List<Author> authors = authorService.readAuthorListPerPage(pageNumber);

        verify(authorDAO, times(1)).readAuthorListPerPage(startPage, endPosition);
    }

    @Test
    public void testReadAuthorList() throws Exception {
        authorService.readAuthorList();

        verify(authorDAO, times(1)).readAuthorList();
    }

    @Test
    public void testReadAuthorForNews() throws Exception {
        Long newsId = 1L;

        Author author = authorService.readAuthorForNews(newsId);

        verify(authorDAO, times(1)).readAuthorForNews(newsId);
    }

    @Test
    public void testMakeExpired() throws Exception {
        Date expiredAuthorDate = new Date();
        Author author = new Author();
        author.setExpiredDate(expiredAuthorDate);

        authorService.makeExpired(author);

        verify(authorDAO, times(1)).makeExpired(author);
    }
}