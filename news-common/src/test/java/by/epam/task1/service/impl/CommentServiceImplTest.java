package by.epam.task1.service.impl;

import by.epam.task1.dao.CommentDAO;
import by.epam.task1.domain.Comment;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/spring-context.xml"})
public class CommentServiceImplTest {

    @Mock
    private CommentDAO commentDAO;

    @InjectMocks
    private CommentServiceImpl commentService;

    @Test
    public void testCreate() throws Exception {
        Long expectedCommentId = 1L;

        Comment comment = new Comment();
        when(commentService.create(comment)).thenReturn(expectedCommentId);

        Long commentId = commentService.create(comment);
        verify(commentDAO, times(1)).create(comment);

        assertEquals(commentId, expectedCommentId);
    }

    @Test
    public void testRead() throws Exception {
        Long commentId = 1L;
        String commentText = "testCommentText";

        Comment expectedComment = new Comment();
        expectedComment.setId(commentId);
        expectedComment.setText(commentText);

        when(commentService.read(commentId)).thenReturn(expectedComment);

        Comment actualComment = commentService.read(commentId);

        verify(commentDAO, times(1)).read(commentId);
        assertEquals(actualComment.getId(), expectedComment.getId());
        assertEquals(actualComment.getText(), expectedComment.getText());
    }

    @Test
    public void testUpdate() throws Exception {
        Long commentId = 1L;
        String commentText = "updateCommentText";

        Comment comment = new Comment();
        comment.setId(commentId);
        comment.setText(commentText);

        commentService.update(comment);

        verify(commentDAO, times(1)).update(comment);
    }

    @Test
    public void testDelete() throws Exception {
        Long commentId = 1L;

        commentService.delete(commentId);

        verify(commentDAO, times(1)).delete(commentId);
    }

    @Test
    public void testReadCommentsForNews() throws Exception {
        Long newsId = 1L;

        List<Comment> comments = commentService.readCommentsForNews(newsId);

        verify(commentDAO, times(1)).readCommentListForNews(newsId);
    }

    @Test
    public void testDeleteCommentsForNews() throws Exception {
        Long newsId = 1L;

        commentService.deleteCommentsForNews(newsId);

        verify(commentDAO, times(1)).deleteCommentsForNews(newsId);
    }
}