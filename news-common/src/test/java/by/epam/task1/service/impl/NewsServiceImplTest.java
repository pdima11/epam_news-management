package by.epam.task1.service.impl;

import by.epam.task1.dao.NewsDAO;
import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/spring-context.xml"})

public class NewsServiceImplTest {

    @Mock
    private NewsDAO newsDAO;

    @InjectMocks
    private NewsServiceImpl newsService;


    @Test
    public void testCreate() throws Exception {
        Long expectedNewsId = 1L;

        News news = new News();
        when(newsService.create(news)).thenReturn(expectedNewsId);

        Long authorId = newsService.create(news);
        verify(newsDAO, times(1)).create(news);

        assertEquals(authorId, expectedNewsId);
    }

    @Test
    public void testRead() throws Exception {
        Long newsId = 1L;
        String newsTitle = "testNewsTitle";

        News expectedNews = new News();
        expectedNews.setId(newsId);
        expectedNews.setTitle(newsTitle);

        when(newsService.read(newsId)).thenReturn(expectedNews);

        News actualNews = newsService.read(newsId);

        verify(newsDAO, times(1)).read(newsId);
        assertEquals(expectedNews.getId(), actualNews.getId());
        assertEquals(expectedNews.getTitle(), actualNews.getTitle());
    }

    @Test
    public void testUpdate() throws Exception {
        Long newsId = 1L;
        String newsTitle = "updateNewsTitle";

        News news = new News();
        news.setId(newsId);
        news.setTitle(newsTitle);

        newsService.update(news);

        verify(newsDAO, times(1)).update(news);
    }

    @Test
    public void testDelete() throws Exception {
        Long authorId = 1L;

        newsService.delete(authorId);

        verify(newsDAO, times(1)).delete(authorId);
    }

    @Test
    public void testSearch() throws Exception {
        SearchCriteriaTO searchCriteria = new SearchCriteriaTO();

        int pageNumber = 1;
        int startPosition = 1;
        int endPosition = 8;

        List<News> newsList = newsService.search(pageNumber, searchCriteria);

        verify(newsDAO, times(1)).search(startPosition, endPosition, searchCriteria);
    }

    @Test
    public void testReadNewsListSortedByComments() throws Exception {
        int pageNumber = 3;
        int startPosition = 15;
        int endPosition = 22;

        List<News> news = newsService.readNewsListSortedByComments(pageNumber);

        verify(newsDAO, times(1)).readNewsListSortedByComments(startPosition, endPosition);
    }

    @Test
    public void testReadNewsListPerPage() throws Exception {
        int pageNumber = 2;
        int startPosition = 8;
        int endPosition = 15;

        List<News> news = newsService.readNewsListPerPage(pageNumber);

        verify(newsDAO, times(1)).readNewsListPerPage(startPosition, endPosition);
    }

    @Test
    public void testCountAllNews() throws Exception {
        long numberOfNews = newsService.getNumberOfAllNews();

        verify(newsDAO, times(1)).getNumberOfAllNews();
    }

    @Test
    public void testCreateBindWithAuthor() throws Exception {
        Long newsId = 1L;
        Long authorId = 1L;

        newsService.createBindWithAuthor(newsId, authorId);

        verify(newsDAO, times(1)).createBindNewsWithAuthor(newsId, authorId);
    }

    @Test
    public void testCreateBindWithTags() throws Exception {
        Long newsId = 1L;
        Long tagId = 1L;

        List<Long> tagIdList = new ArrayList<>();
        tagIdList.add(tagId);

        newsService.createBindWithTags(newsId, tagIdList);

        verify(newsDAO, times(1)).createBindNewsWithTags(newsId, tagIdList);
    }

    @Test
    public void testDeleteBindWithAuthor() throws Exception {
        Long newsId = 1L;

        newsService.deleteBindWithAuthor(newsId);

        verify(newsDAO, times(1)).deleteBindNewsWithAuthor(newsId);
    }

    @Test
    public void testDeleteBindWithTags() throws Exception {
        Long newsId = 1L;
        Long tagId = 1L;

        List<Long> tagIdList = new ArrayList<>();
        tagIdList.add(tagId);

        newsService.deleteBindWithTags(newsId);

        verify(newsDAO, times(1)).deleteBindNewsWithTags(newsId);
    }
}