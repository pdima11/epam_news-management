package by.epam.task1.service.impl;

import by.epam.task1.dao.TagDAO;

import by.epam.task1.domain.Tag;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = "/spring-context.xml")
public class TagServiceImplTest {

    @Mock
    private TagDAO tagDAO;

    @InjectMocks
    private TagServiceImpl tagService;

    @Test
    public void testCreate() throws Exception {
        Long expectedTagId = 1L;

        Tag tag= new Tag();
        when(tagService.create(tag)).thenReturn(expectedTagId);

        Long actualTagId = tagService.create(tag);

        verify(tagDAO, times(1)).create(tag);
        assertEquals(expectedTagId, actualTagId);
    }

    @Test
    public void testRead() throws Exception {
        Long tagId = 1L;
        String tagName = "testTagName";

        Tag expectedTag = new Tag();
        expectedTag.setId(tagId);
        expectedTag.setName(tagName);

        when(tagService.read(tagId)).thenReturn(expectedTag);

        Tag actualTag = tagService.read(tagId);

        verify(tagDAO, times(1)).read(tagId);

        assertEquals(expectedTag.getId(), actualTag.getId());
        assertEquals(expectedTag.getName(), actualTag.getName());
    }

    @Test
    public void testUpdate() throws Exception {
        Long tagId = 1L;
        String tagName = "updateAuthorName";

        Tag tag = new Tag();
        tag.setId(tagId);
        tag.setName(tagName);

        tagService.update(tag);

        verify(tagDAO, times(1)).update(tag);
    }

    @Test
    public void testDelete() throws Exception {
        Long tagId = 1L;

        tagService.delete(tagId);

        verify(tagDAO, times(1)).delete(tagId);
    }

    @Test
    public void testReadTagsForNews() throws Exception {
        Long newsId = 1L;

        List<Tag> tags = tagService.readTagsForNews(newsId);

        verify(tagDAO, times(1)).readTagsForNews(newsId);
    }

    @Test
    public void testReadTagListPerPage() throws Exception {
        int pageNumber = 1;
        int startPosition = 1;
        int endPosition = 12;

        List<Tag> tags = tagService.readTagListPerPage(pageNumber);

        verify(tagDAO, times(1)).readTagListPerPage(startPosition, endPosition);
    }

    @Test
    public void testReadTagList() throws Exception {
        List<Tag> tags = tagService.readTagList();

        verify(tagDAO, times(1)).readTagList();
    }
}