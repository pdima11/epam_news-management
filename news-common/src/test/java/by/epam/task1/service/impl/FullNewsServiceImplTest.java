package by.epam.task1.service.impl;

import by.epam.task1.domain.*;
import by.epam.task1.service.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/spring-context.xml"})
public class FullNewsServiceImplTest {

    @Mock
    private AuthorService authorService;

    @Mock
    private CommentService commentService;

    @Mock
    private NewsService newsService;

    @Mock
    private TagService tagService;

    @InjectMocks
    private FullNewsServiceImpl fullNewsService;

    @Test
    public void testCreate() throws Exception {
        Long authorId = 1L;
        Long newsId = 1L;

        FullNewsTO fullNews = new FullNewsTO();

        News news = new News();

        Author author= new Author();
        author.setId(authorId);

        List<Tag> tags = new ArrayList<>();

        fullNews.setNews(news);
        fullNews.setAuthor(author);
        fullNews.setTags(tags);

        List<Long> tagIdList = new ArrayList<>();
        for(Tag tag: tags) {
            tagIdList.add(tag.getId());
        }

        when(newsService.create(news)).thenReturn(newsId);
        fullNewsService.create(fullNews);

        verify(newsService, times(1)).create(news);
        verify(newsService, times(1)).createBindWithAuthor(newsId, author.getId());
        verify(newsService, times(1)).createBindWithTags(newsId, tagIdList);
    }

    @Test
    public void testDelete() throws Exception {
        Long newsId = 1L;

        FullNewsTO fullNews = new FullNewsTO();
        News news = new News();
        Author author= new Author();
        Tag tag = new Tag();

        news.setId(newsId);

        List<Long> tagIdList = new ArrayList<>();
        tagIdList.add(tag.getId());

        List<Tag> tags = new ArrayList<>();
        tags.add(tag);

        fullNews.setNews(news);
        fullNews.setAuthor(author);
        fullNews.setTags(tags);

        fullNewsService.delete(newsId);

        verify(newsService, times(1)).deleteBindWithTags(fullNews.getNews().getId());
        verify(newsService, times(1)).deleteBindWithAuthor(fullNews.getNews().getId());
        verify(commentService, times(1)).deleteCommentsForNews(fullNews.getNews().getId());
    }

    @Test
    public void testRead() throws Exception {
        Long newsId = 1L;

        News news = new News();
        news.setId(newsId);
        when(newsService.read(newsId)).thenReturn(news);

        FullNewsTO fullNews = fullNewsService.read(newsId);

        verify(newsService, times(1)).read(newsId);
        verify(authorService, times(1)).readAuthorForNews(newsId);
        verify(commentService, times(1)).readCommentsForNews(newsId);
        verify(tagService, times(1)).readTagsForNews(newsId);
    }

    @Test
    public void testSearch() throws Exception {
        int pageNumber = 1;
        Long newsId1 = 1L;
        Long newsId2 = 2L;

        SearchCriteriaTO searchCriteria = new SearchCriteriaTO();

        News news1 = new News();
        news1.setId(newsId1);
        News news2 = new News();
        news2.setId(newsId2);

        List<News> expectedNewsList = new ArrayList<>();
        expectedNewsList.add(news1);
        expectedNewsList.add(news2);

        when(newsService.search(pageNumber, searchCriteria)).thenReturn(expectedNewsList);
        List<FullNewsTO> fullNewsList = fullNewsService.search(pageNumber, searchCriteria);

        verify(newsService, times(1)).search(pageNumber, searchCriteria);
        for(News news: expectedNewsList) {
            verify(authorService, times(1)).readAuthorForNews(news.getId());
            verify(commentService, times(1)).readCommentsForNews(news.getId());
            verify(tagService, times(1)).readTagsForNews(news.getId());
        }
    }

    @Test
    public void testReadNewsList() throws Exception {
        int pageNumber = 1;
        Long newsId1 = 1L;
        Long newsId2 = 2L;

        News news1 = new News();
        news1.setId(newsId1);
        News news2 = new News();
        news2.setId(newsId2);

        List<News> expectedNewsList = new ArrayList<>();
        expectedNewsList.add(news1);
        expectedNewsList.add(news2);

        when(newsService.readNewsListPerPage(pageNumber)).thenReturn(expectedNewsList);
        List<FullNewsTO> fullNewsList = fullNewsService.readNewsList(pageNumber);

        verify(newsService, times(1)).readNewsListPerPage(pageNumber);
        for(News news: expectedNewsList) {
            verify(authorService, times(1)).readAuthorForNews(news.getId());
            verify(commentService, times(1)).readCommentsForNews(news.getId());
            verify(tagService, times(1)).readTagsForNews(news.getId());
        }
    }

    @Test
    public void testReadNewsListSortedByComments() throws Exception {
        int pageNumber = 1;
        Long newsId1 = 1L;
        Long newsId2 = 2L;

        News news1 = new News();
        news1.setId(newsId1);
        News news2 = new News();
        news2.setId(newsId2);

        List<News> expectedNewsList = new ArrayList<>();
        expectedNewsList.add(news1);
        expectedNewsList.add(news2);

        when(newsService.readNewsListSortedByComments(pageNumber)).thenReturn(expectedNewsList);
        List<FullNewsTO> fullNewsList = fullNewsService.readNewsListSortedByComments(pageNumber);

        verify(newsService, times(1)).readNewsListSortedByComments(pageNumber);
        for(News news: expectedNewsList) {
            verify(authorService, times(1)).readAuthorForNews(news.getId());
            verify(commentService, times(1)).readCommentsForNews(news.getId());
            verify(tagService, times(1)).readTagsForNews(news.getId());
        }
    }
}