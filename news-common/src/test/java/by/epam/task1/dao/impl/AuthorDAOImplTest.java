package by.epam.task1.dao.impl;

import by.epam.task1.dao.AuthorDAO;
import by.epam.task1.domain.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.inject.Inject;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Dzmitry_Padvalnikau on 2/16/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(locations = {"/spring-context.xml"})
@DatabaseSetup(value = "AuthorDAOImplTest.xml")
@DatabaseTearDown(value = "AuthorDAOImplTest.xml", type = DatabaseOperation.DELETE_ALL)
public class AuthorDAOImplTest {

    @Inject
    private AuthorDAO authorDAO;

    @Test
    public void testCreate() throws Exception {
        Long authorId = null;
        String testAuthorName = "testAuthorName";

        Author author1 = new Author();
        author1.setName(testAuthorName);

        authorId = authorDAO.create(author1);

        Author author2 = authorDAO.read(authorId);

        assertEquals(author1.getName(), author2.getName());
    }

    @Test
    public void testRead() throws Exception {
        Long authorId = 3L;
        String authorName = "testAuthorName3";

        Author author = authorDAO.read(authorId);

        assertEquals(author.getName(), authorName);
    }

    @Test
    public void testUpdate() throws Exception {
        Long authorId = 2L;
        String updateAuthorName = "testUpdateAuthorName";

        Author author1 = new Author();
        author1.setId(authorId);
        author1.setName(updateAuthorName);
        authorDAO.update(author1);

        Author author2 = authorDAO.read(authorId);

        assertEquals(author1.getName(), author2.getName());
    }

    @Test
    public void testDelete() throws Exception {
        Long authorId = 4L;

        Author author = authorDAO.read(authorId);

        assertNotNull(author);

        authorDAO.delete(authorId);
        author = authorDAO.read(authorId);

        assertNull(author);
    }

    @Test
    public void testMakeExpired() throws Exception {
        Long authorId = 1L;
        Date expiredDate = new Date();

        Author author = new Author();
        author.setId(authorId);
        author.setExpiredDate(expiredDate);

        authorDAO.makeExpired(author);

        author = authorDAO.read(authorId);

        assertEquals(expiredDate, author.getExpiredDate());
    }

    @Test
    public void testReadAuthorForNews() throws Exception {
        Long newsId = 1L;
        String authorName = "testAuthorName1";

        Author author = authorDAO.readAuthorForNews(newsId);

        assertEquals(authorName, author.getName());
    }

    @Test
    public void testReadAuthorListPerPage() throws Exception {
        int pageNumber = 1;
        int numberOfItemsOnPage = 2;

        int endPosition = (pageNumber * numberOfItemsOnPage) + 1;
        int startPosition = endPosition - numberOfItemsOnPage;

        String authorName1 = "testAuthorName1";
        String authorName2 = "testAuthorName2";

        List<Author> authors = authorDAO.readAuthorListPerPage(startPosition, endPosition);

        Author author1 = authors.get(0);
        Author author2 = authors.get(1);

        assertEquals(author1.getName(), authorName1);
        assertEquals(author2.getName(), authorName2);
    }

    @Test
    public void testReadAuthorList() throws Exception {
        String authorName1 = "testAuthorName1";
        String authorName2 = "testAuthorName2";
        String authorName3 = "testAuthorName3";

        List<Author> authors = authorDAO.readAuthorList();

        Author author1 = authors.get(0);
        Author author2 = authors.get(1);
        Author author3 = authors.get(2);

        assertEquals(author1.getName(), authorName1);
        assertEquals(author2.getName(), authorName2);
        assertEquals(author3.getName(), authorName3);
    }
}