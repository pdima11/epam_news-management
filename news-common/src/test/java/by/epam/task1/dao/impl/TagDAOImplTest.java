package by.epam.task1.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import by.epam.task1.dao.TagDAO;

import by.epam.task1.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/15/2016.
 */


@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@ContextConfiguration(locations = {"/spring-context.xml"})
@DatabaseSetup(value = "TagDAOImplTest.xml")
@DatabaseTearDown(value = "TagDAOImplTest.xml", type = DatabaseOperation.DELETE_ALL)
public class TagDAOImplTest {

    @Inject
    private TagDAO tagDAO;

    @Test
    public void testCreate() throws Exception {
        Long tagId = null;
        String testTagName = "testTagName";

        Tag tag1 = new Tag();
        tag1.setName(testTagName);

        tagId = tagDAO.create(tag1);

        Tag tag2 = tagDAO.read(tagId);

        assertEquals(tag1.getName(), tag2.getName());
    }

    @Test
    public void testRead() throws Exception {
        Long tagId = 3L;
        String tagName = "testTagName3";

        Tag tag = tagDAO.read(tagId);

        assertEquals(tag.getName(), tagName);
    }

    @Test
    public void testUpdate() throws Exception {
        Long tagId = 2L;
        String updateTagName = "testUpdateTagName";

        Tag tag1 = new Tag();
        tag1.setId(tagId);
        tag1.setName(updateTagName);
        tagDAO.update(tag1);

        Tag tag2 = tagDAO.read(tagId);

        assertEquals(tag1.getName(), tag2.getName());
    }

    @Test
    public void testDelete() throws Exception {
        Long tagId = 2L;

        Tag tag = tagDAO.read(tagId);
        assertNotNull(tag);

        tagDAO.deleteBindTagWithNews(tagId);
        tagDAO.delete(tagId);
        tag = tagDAO.read(tagId);
        assertNull(tag);
    }

    @Test
    public void testDeleteBindTagWithNews() throws Exception {
        Long tagId = 3L;
        Long newsId = 3L;
        int numberOfTagsBindingWithNews = 0;

        tagDAO.deleteBindTagWithNews(tagId);

        List<Tag> tags = tagDAO.readTagsForNews(newsId);

        assertEquals(tags.size(), numberOfTagsBindingWithNews);
    }

    @Test
    public void testReadTagsForNews() throws Exception {
        Long newsId = 2L;
        String tagName = "testTagName2";

        List<Tag> tags = tagDAO.readTagsForNews(newsId);
        Tag tag = tags.get(0);

        assertEquals(tag.getName(), tagName);
    }

    @Test
    public void testReadTagListPerPage() throws Exception {
        int pageNumber = 1;
        int numberOfItemsOnPage = 2;

        int endPosition = (pageNumber * numberOfItemsOnPage) + 1;
        int startPosition = endPosition - numberOfItemsOnPage;

        String tagName1 = "testTagName1";
        String tagName2 = "testTagName2";

        List<Tag> tags = tagDAO.readTagListPerPage(startPosition, endPosition);

        Tag tag1 = tags.get(0);
        Tag tag2 = tags.get(1);

        assertEquals(tag1.getName(), tagName1);
        assertEquals(tag2.getName(), tagName2);
    }

    @Test
    public void testReadTagList() throws Exception {
        String tagName1 = "testTagName1";
        String tagName2 = "testTagName2";
        String tagName3 = "testTagName3";

        List<Tag> tags = tagDAO.readTagList();

        Tag tag1 = tags.get(0);
        Tag tag2 = tags.get(1);
        Tag tag3 = tags.get(2);

        assertEquals(tag1.getName(), tagName1);
        assertEquals(tag2.getName(), tagName2);
        assertEquals(tag3.getName(), tagName3);
    }
}