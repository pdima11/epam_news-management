package by.epam.task1.dao.impl;


import by.epam.task1.dao.AuthorDAO;
import by.epam.task1.dao.NewsDAO;
import by.epam.task1.dao.TagDAO;
import by.epam.task1.domain.Author;
import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Dzmitry_Padvalnikau on 2/16/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(locations = {"/spring-context.xml"})
@DatabaseSetup(value = "NewsDAOImplTest.xml")
@DatabaseTearDown(value = "NewsDAOImplTest.xml", type = DatabaseOperation.DELETE_ALL)
public class NewsDAOImplTest {

    @Inject
    private NewsDAO newsDAO;

    @Inject
    private AuthorDAO authorDAO;

    @Inject
    private TagDAO tagDAO;

    @Test
    public void testCreate() throws Exception {
        Long newsId = null;
        String newsTitle  = "testNewsTitle";
        String newsShortText = "testNewsShortText";
        String newsFullText = "testNewsFullText";

        News news1 = new News();
        news1.setTitle(newsTitle);
        news1.setShortText(newsShortText);
        news1.setFullText(newsFullText);

        newsId = newsDAO.create(news1);

        News news2 = newsDAO.read(newsId);

        assertEquals(news1.getTitle(), news2.getTitle());
        assertEquals(news1.getShortText(), news2.getShortText());
        assertEquals(news1.getFullText(), news2.getFullText());
    }

    @Test
    public void testRead() throws Exception {
        Long newsId = 1L;
        String newsTitle = "testNewsTitle1";

        News news = newsDAO.read(newsId);

        assertEquals(newsTitle, news.getTitle());
    }

    @Test
    public void testUpdate() throws Exception {
        Long newsId = 1L;
        String updateNewsTitle = "testUpdateNewsTitle";
        String updateNewsShortText = "testUpdateNewsShortText";
        String updateNewsFullText = "testUpdateNewsFullText";

        News news1 = new News();
        news1.setId(newsId);
        news1.setTitle(updateNewsTitle);
        news1.setShortText(updateNewsShortText);
        news1.setFullText(updateNewsFullText);

        newsDAO.update(news1);

        News news2 = newsDAO.read(newsId);

        assertEquals(news1.getTitle(), news2.getTitle());
    }

    @Test
    public void testDelete() throws Exception {
        Long newsId = 4L;

        News news = newsDAO.read(newsId);
        assertNotNull(news);

        newsDAO.delete(newsId);
        news = newsDAO.read(newsId);
        assertNull(news);
    }

    @Test
    public void testSearch() throws Exception {
        Long authorId = 1L;
        Long tagId1 = 1L;
        Long tagId2 = 2L;
        String expectedNewsTitle = "testNewsTitle1";

        int pageNumber = 1;
        int numberOfItemsOnPage = 1;

        int endPosition = (pageNumber * numberOfItemsOnPage) + 1;
        int startPosition = endPosition - numberOfItemsOnPage;

        List<Long> tagIdList = new ArrayList<>();
        tagIdList.add(tagId1);
        tagIdList.add(tagId2);

        SearchCriteriaTO searchCriteria = new SearchCriteriaTO();
        searchCriteria.setAuthorId(authorId);
        searchCriteria.setTagIdList(tagIdList);

        List<News> newsList = newsDAO.search(startPosition, endPosition, searchCriteria);

        assertEquals(newsList.get(0).getTitle(), expectedNewsTitle);
    }

    @Test
    public void testReadNewsListSortedByComments() throws Exception {
        int pageNumber = 1;
        int numberOfItemsOnPage = 3;

        int endPosition = (pageNumber * numberOfItemsOnPage) + 1;
        int startPosition = endPosition - numberOfItemsOnPage;

        String newsTitle1 = "testNewsTitle1";
        String newsTitle2 = "testNewsTitle3";
        String newsTitle3 = "testNewsTitle2";

        List<News> newsList = newsDAO.readNewsListSortedByComments(startPosition, endPosition);

        News news1 = newsList.get(0);
        News news2 = newsList.get(1);
        News news3 = newsList.get(2);

        assertEquals(news1.getTitle(), newsTitle1);
        assertEquals(news2.getTitle(), newsTitle2);
        assertEquals(news3.getTitle(), newsTitle3);
    }

    @Test
    public void testCountAllNews() throws Exception {
        long numberOfNews = 4;
        long testNumberOfNews = newsDAO.getNumberOfAllNews();

        assertEquals(testNumberOfNews, numberOfNews);
    }

    @Test
    public void testCreateBindNewsWithAuthor() throws Exception {
        Long newsId = 4L;
        Long authorId = 4L;

        Author author1 = authorDAO.readAuthorForNews(newsId);
        assertNull(author1);

        newsDAO.createBindNewsWithAuthor(newsId, authorId);
        author1 = authorDAO.readAuthorForNews(newsId);
        assertNotNull(author1);
    }

    @Test
    public void testCreateBindNewsWithTags() throws Exception {
        Long newsId = 4L;
        Long tagId = 1L;

        int numberOfTagWithoutBind = 0;
        int numberOfTagWithBind = 1;

        News news = new News();
        news.setId(newsId);

        List<Tag> tags = tagDAO.readTagsForNews(newsId);
        assertEquals(tags.size(), numberOfTagWithoutBind);

        List<Long> tagIdListForCreateBind = new ArrayList<>();
        tagIdListForCreateBind.add(tagId);

        newsDAO.createBindNewsWithTags(newsId, tagIdListForCreateBind);
        tags = tagDAO.readTagsForNews(newsId);

        assertEquals(tags.size(), numberOfTagWithBind);
        assertEquals(tags.get(0).getId(), tagId);
    }

    @Test
    public void testDeleteBindNewsWithAuthor() throws Exception {
        Long newsId = 1L;

        News news = new News();
        news.setId(newsId);

        Author author = authorDAO.readAuthorForNews(newsId);
        assertNotNull(author);

        newsDAO.deleteBindNewsWithAuthor(news.getId());

        author = authorDAO.readAuthorForNews(newsId);
        assertNull(author);
    }

    @Test
    public void testDeleteBindNewsWithTags() throws Exception {
        Long newsId = 1L;
        Long tagId1 = 1L;
        Long tagId2 = 2L;

        int numberOfTagWithoutBind = 0;
        int numberOfTagWithBind = 2;


        List<Tag> tags = tagDAO.readTagsForNews(newsId);
        assertEquals(tags.size(), numberOfTagWithBind);

        newsDAO.deleteBindNewsWithTags(newsId);
        tags = tagDAO.readTagsForNews(newsId);

        assertEquals(tags.size(), numberOfTagWithoutBind);
    }

    @Test
    public void testReadNewsListPerPage() throws Exception {
        int pageNumber = 1;
        int numberOfItemOnPage = 2;

        int endPosition = (pageNumber * numberOfItemOnPage) + 1;
        int startPosition = endPosition - numberOfItemOnPage;

        String newsTitle1 = "testNewsTitle3";
        String newsTitle2 = "testNewsTitle2";

        List<News> newsList = newsDAO.readNewsListPerPage(startPosition, endPosition);

        News news1 = newsList.get(0);
        News news2 = newsList.get(1);

        assertEquals(news1.getTitle(), newsTitle1);
        assertEquals(news2.getTitle(), newsTitle2);
    }
}