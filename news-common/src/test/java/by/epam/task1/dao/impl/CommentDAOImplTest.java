package by.epam.task1.dao.impl;

import by.epam.task1.dao.CommentDAO;

import by.epam.task1.domain.Comment;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Dzmitry_Padvalnikau on 2/16/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@ContextConfiguration(locations = {"/spring-context.xml"})
@DatabaseSetup(value = "CommentDAOImplTest.xml")
@DatabaseTearDown(value = "CommentDAOImplTest.xml", type = DatabaseOperation.DELETE_ALL)
public class CommentDAOImplTest {

    @Inject
    private CommentDAO commentDAO;

    @Test
    public void testCreate() throws Exception {
        String commentText  = "testCommentText";
        Long newsId = 1L;

        Comment comment1 = new Comment();
        comment1.setNewsId(newsId);
        comment1.setText(commentText);

        Long commentId = commentDAO.create(comment1);

        Comment comment2 = commentDAO.read(commentId);

        assertEquals(comment1.getNewsId(), comment2.getNewsId());
        assertEquals(comment1.getText(), comment2.getText());
    }

    @Test
    public void testRead() throws Exception {
        Long commentId = 1L;
        Long newsId = 1L;
        String commentText = "testCommentText1";

        Comment comment = commentDAO.read(commentId);

        assertEquals(newsId, comment.getNewsId());
        assertEquals(commentText, comment.getText());
    }

    @Test
    public void testUpdate() throws Exception {
        Long commentId = 2L;
        String updateCommentText = "testUpdateCommentText";

        Comment comment1 = new Comment();
        comment1.setId(commentId);
        comment1.setText(updateCommentText);
        commentDAO.update(comment1);

        Comment comment2 = commentDAO.read(commentId);

        assertEquals(comment1.getText(), comment2.getText());
    }

    @Test
    public void testDelete() throws Exception {
        Long commentId = 2L;

        Comment comment = commentDAO.read(commentId);
        assertNotNull(comment);

        commentDAO.delete(commentId);
        comment = commentDAO.read(commentId);
        assertNull(comment);
    }

    @Test
    public void testReadCommentListForNews() throws Exception {
        Long newsId = 1L;
        String commentText1 = "testCommentText1";
        String commentText2 = "testCommentText2";

        List<Comment> comments = commentDAO.readCommentListForNews(newsId);
        Comment comment1 = comments.get(0);
        Comment comment2 = comments.get(1);

        assertEquals(comment1.getNewsId(), newsId);
        assertEquals(comment1.getText(), commentText1);

        assertEquals(comment2.getNewsId(), newsId);
        assertEquals(comment2.getText(), commentText2);
    }

    @Test
    public void testDeleteCommentsForNews() throws Exception {
        Long newsId = 3L;
        int numberOfCommentsWithNewsIdBeforeTest = 1;
        int numberOfCommentsWithNewsIdAfterTest = 0;

        List<Comment> comments = commentDAO.readCommentListForNews(newsId);

        assertEquals(comments.size(), numberOfCommentsWithNewsIdBeforeTest);

        commentDAO.deleteCommentsForNews(newsId);
        comments = commentDAO.readCommentListForNews(newsId);

        assertEquals(comments.size(), numberOfCommentsWithNewsIdAfterTest);
    }
}