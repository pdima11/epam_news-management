package by.epam.task1.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/9/2016.
 */
public class SearchCriteriaTO implements Serializable{
    private static final long serialVersionUID = 1L;

    private Long authorId;
    private List<Long> tagIdList;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagIdList() {
        return tagIdList;
    }

    public void setTagIdList(List<Long> tagIdList) {
        this.tagIdList = tagIdList;
    }
}
