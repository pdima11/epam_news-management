package by.epam.task1.domain;

import javax.validation.constraints.Size;
import java.io.Serializable;


public class Tag implements Serializable{
    private static final long serialVersionUID = 1L;

    private Long id;

    @Size(min = 2, max = 25)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (this.getClass() != obj.getClass()) {
            return false;
        }

        Tag tag = (Tag) obj;

        if (this.id != tag.id) {
            return false;
        }

        if (this.name != tag.name) {
            if (!this.name.equals(tag.name)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;

        if (this.id == null) {
            result = 0;
        } else {
            result = 7 * this.id.hashCode();
        }

        if (this.name == null) {
            result += 0;
        } else {
            result += 11 * this.name.hashCode();
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(": id = ");
        sb.append(this.id);
        sb.append(", name = ");
        sb.append(this.name);

        return sb.toString();
    }
}
