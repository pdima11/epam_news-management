package by.epam.task1.domain;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;


public class Comment implements Serializable{
    private static final long serialVersionUID = 1L;

    private Long id;

    private Long newsId;

    @Size(min = 1,  max = 250)
    private String text;

    private Date creationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (this.getClass() != obj.getClass()) {
            return false;
        }

        Comment comment = (Comment) obj;

        if (this.id != comment.id) {
            return false;
        }

        if (this.newsId != comment.newsId) {
            return false;
        }

        if (this.text != comment.text) {
            if (!this.text.equals(comment.text)) {
                return false;
            }
        }

        if (this.creationDate != comment.creationDate) {
            if (!this.creationDate.equals(comment.creationDate)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;

        if (this.id == null) {
            result = 0;
        } else {
            result = 3 * this.id.hashCode();
        }

        if (this.newsId == null) {
            result += 0;
        } else {
            result += 5 * this.newsId.hashCode();
        }

        if (this.text == null) {
            result += 0;
        } else {
            result += 7 * this.text.hashCode();

        }

        if (this.creationDate == null) {
            result += 0;
        } else {
            result += 11 * this.creationDate.hashCode();
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(": id = ");
        sb.append(this.id);
        sb.append(", newsId = ");
        sb.append(this.newsId);
        sb.append(", text = ");
        sb.append(this.text);
        sb.append(", creationDate = ");
        sb.append(this.creationDate);

        return sb.toString();
    }
}
