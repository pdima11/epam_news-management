package by.epam.task1.domain;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;


public class Author implements Serializable{
    private static final long serialVersionUID = 1L;

    private Long id;
    
    @Size(min = 2, max = 50)
    private String name;

    private Date expiredDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (this.getClass() != obj.getClass()) {
            return false;
        }

        Author author = (Author) obj;

        if (this.id != author.id) {
            return false;
        }

        if (this.name != author.name) {
            if (!this.name.equals(author.name)) {
                return false;
            }
        }

        if (this.expiredDate != author.expiredDate) {
            if (!this.expiredDate.equals(author.expiredDate)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;

        if (this.id == null) {
            result = 0;
        } else {
            result = 3 * this.id.hashCode();
        }

        if (this.name == null) {
            result += 0;
        } else {
            result += 7 * this.name.hashCode();
        }

        if (this.expiredDate == null) {
            result += 0;
        } else {
            result += 11 * this.expiredDate.hashCode();
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(": id = ");
        sb.append(this.id);
        sb.append(", name = ");
        sb.append(this.name);
        sb.append(", expiredDate = ");
        sb.append(this.expiredDate);

        return sb.toString();
    }
}
