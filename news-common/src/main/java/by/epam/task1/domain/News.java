package by.epam.task1.domain;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Dzmitry_Padvalnikau on 2/9/2016.
 */
public class News implements Serializable{
    private static final long serialVersionUID = 1L;

    private Long id;

    @Size(min = 10, max = 250)
    private String title;

    @Size(min = 10, max = 500)
    private String shortText;

    @Size(min = 10, max = 1000)
    private String fullText;

    private Date creationDate;

    private Date modificationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (this.getClass() != obj.getClass()) {
            return false;
        }

        News news = (News) obj;

        if (this.id != news.id) {
            return false;
        }

        if (this.title != news.title) {
            if (!this.title.equals(news.title)) {
                return false;
            }
        }

        if (this.shortText != news.shortText) {
            if (!this.shortText.equals(news.shortText)) {
                return false;
            }
        }

        if (this.fullText != news.fullText) {
            if (!this.fullText.equals(news.fullText)) {
                return false;
            }
        }

        if (this.creationDate != news.creationDate) {
            if (!this.creationDate.equals(news.creationDate)) {
                return false;
            }
        }

        if (this.modificationDate != news.modificationDate) {
            if (!this.modificationDate.equals(news.modificationDate)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;

        if (this.id == null) {
            result = 0;
        } else {
            result = 3 * this.id.hashCode();
        }

        if (this.shortText == null) {
            result += 0;
        } else {
            result += 5 * this.title.hashCode();
        }

        if (this.fullText == null) {
            result += 0;
        } else {
            result += 7 * this.fullText.hashCode();
        }

        if (this.creationDate == null) {
            result += 0;
        } else {
            result += 11 * this.creationDate.hashCode();
        }

        if (this.modificationDate == null) {
            result += 0;
        } else {
            result += 11 * this.modificationDate.hashCode();
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(": id = ");
        sb.append(this.id);
        sb.append(", title = ");
        sb.append(this.title);
        sb.append(", shortText = ");
        sb.append(this.shortText);
        sb.append(", fullText = ");
        sb.append(this.fullText);
        sb.append(", creationDate =");
        sb.append(this.creationDate);
        sb.append(", modificationDate = ");
        sb.append(this.modificationDate);

        return sb.toString();
    }
}
