package by.epam.task1.exception;

/**
 * Created by Dzmitry_Padvalnikau on 2/9/2016.
 */
public class ServiceException extends Exception{
    private static final long serialVersionUID = 1L;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Exception e) {
        super(message, e);
    }
}
