package by.epam.task1.service.impl;

import by.epam.task1.domain.*;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.*;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/16/2016.
 */
@Named
public class FullNewsServiceImpl implements FullNewsService {

    @Inject
    private NewsService newsService;

    @Inject
    private AuthorService authorService;

    @Inject
    private TagService tagService;

    @Inject
    private CommentService commentService;


    @Override
    @Transactional(rollbackFor = { Exception.class })
    public Long create(FullNewsTO fullNews) throws ServiceException {
        if (fullNews == null) {
            throw new ServiceException("Null pointer for FullNewsTO");
        }
        News news = fullNews.getNews();
        List<Tag> tags = fullNews.getTags();
        Author author = fullNews.getAuthor();

        List<Long> tagIdList = new ArrayList<>();
        for (Tag tag: tags) {
            tagIdList.add(tag.getId());
        }

        Long newsId = newsService.create(news);
        newsService.createBindWithAuthor(newsId, author.getId());
        newsService.createBindWithTags(newsId, tagIdList);

        return newsId;
    }

    @Override
    @Transactional(rollbackFor = { Exception.class })
    public void delete(Long newsId) throws ServiceException {
        if (newsId == null) {
            throw new ServiceException("Null pointer for FullNewsTO");
        }

        newsService.deleteBindWithAuthor(newsId);
        newsService.deleteBindWithTags(newsId);
        commentService.deleteCommentsForNews(newsId);
        newsService.delete(newsId);
    }

    @Override
    public FullNewsTO read(Long newsId) throws ServiceException {
        News news = newsService.read(newsId);

        if (news == null) {
            throw new ServiceException("Null pointer for News instance from DAO");
        }

        Author author = authorService.readAuthorForNews(newsId);
        List<Tag> tags = tagService.readTagsForNews(newsId);
        List<Comment> comments = commentService.readCommentsForNews(newsId);

        FullNewsTO fullNews = new FullNewsTO();

        fullNews.setNews(news);
        fullNews.setAuthor(author);
        fullNews.setTags(tags);
        fullNews.setComments(comments);

        return fullNews;
    }

    @Override
    public void update(FullNewsTO fullNews) throws ServiceException {
        News news = fullNews.getNews();
        long authorId = fullNews.getAuthor().getId();

        List<Long> tagIdList = new ArrayList<>();
        for (Tag tag: fullNews.getTags()) {
            tagIdList.add(tag.getId());
        }

        newsService.update(news);

        newsService.deleteBindWithAuthor(news.getId());
        newsService.createBindWithAuthor(news.getId(), authorId);

        newsService.deleteBindWithTags(news.getId());
        newsService.createBindWithTags(news.getId(), tagIdList);
    }

    @Override
    public List<FullNewsTO> search(long pageNumber, SearchCriteriaTO searchCriteria) throws ServiceException {
        List<FullNewsTO> fullNewsList = new ArrayList<>();

        List<News> newsList = newsService.search(pageNumber, searchCriteria);

        for (News news: newsList) {
            Author author = authorService.readAuthorForNews(news.getId());
            List<Tag> tags = tagService.readTagsForNews(news.getId());
            List<Comment> comments = commentService.readCommentsForNews(news.getId());

            FullNewsTO fullNews = new FullNewsTO();
            fullNews.setNews(news);
            fullNews.setAuthor(author);
            fullNews.setTags(tags);
            fullNews.setComments(comments);

            fullNewsList.add(fullNews);
        }

        return fullNewsList;
    }

    @Override
    public List<FullNewsTO> readNewsList(long pageNumber) throws ServiceException {
        List<FullNewsTO> fullNewsList = new ArrayList<>();

        List<News> newsList = newsService.readNewsListPerPage(pageNumber);

        for (News news: newsList) {
            Author author = authorService.readAuthorForNews(news.getId());
            List<Tag> tags = tagService.readTagsForNews(news.getId());
            List<Comment> comments = commentService.readCommentsForNews(news.getId());

            FullNewsTO fullNews = new FullNewsTO();
            fullNews.setNews(news);
            fullNews.setAuthor(author);
            fullNews.setTags(tags);
            fullNews.setComments(comments);

            fullNewsList.add(fullNews);
        }

        return fullNewsList;
    }

    @Override
    public List<FullNewsTO> readNewsListSortedByComments(long pageNumber) throws ServiceException {
        List<FullNewsTO> fullNewsList = new ArrayList<>();

        List<News> newsList = newsService.readNewsListSortedByComments(pageNumber);

        for (News news: newsList) {
            Author author = authorService.readAuthorForNews(news.getId());
            List<Tag> tags = tagService.readTagsForNews(news.getId());
            List<Comment> comments = commentService.readCommentsForNews(news.getId());

            FullNewsTO fullNews = new FullNewsTO();
            fullNews.setNews(news);
            fullNews.setAuthor(author);
            fullNews.setTags(tags);
            fullNews.setComments(comments);

            fullNewsList.add(fullNews);
        }

        return fullNewsList;
    }

    @Override
    public long getPagesNumber() throws ServiceException {
        long numberOfAllNews = newsService.getNumberOfAllNews();
        return (long) (Math.ceil((double) numberOfAllNews / NewsServiceImpl.NUMBER_OF_ITEMS_PER_PAGE));
    }

}
