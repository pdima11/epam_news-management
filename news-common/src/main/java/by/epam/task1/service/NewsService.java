package by.epam.task1.service;

import by.epam.task1.domain.Author;
import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.ServiceException;

import java.util.List;

/**
 * interface <code>NewsService</code> working with
 * instance of <code>News</code> and describe
 * its behavior using Data Access layer implementations
 * of interface <code>NewsDao</code>.
 *
 * @author Dzmitry Padvalnikau
 * @see by.epam.task1.dao.NewsDAO
 * @see by.epam.task1.service.NewsService
 * @see by.epam.task1.domain.News
 */
public interface NewsService extends CRUDGenericService<News> {


    /**
     * Gets list of news for requested page
     * and requested instance of <code>SearchCriteriaTO</code>
     *
     * @param pageNumber requested page
     * @param searchCriteria instance of <code>SearchCriteriaTo</code>
     * @return list of news
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.domain.SearchCriteriaTO
     * @see by.epam.task1.exception.DAOException
     */
    List<News> search(long pageNumber, SearchCriteriaTO searchCriteria) throws ServiceException;


    /**
     * Gets list of news that sorted by number
     * of comment for requested page.
     *
     * @param pageNumber requested page
     * @return list of news
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    List<News> readNewsListSortedByComments(long pageNumber) throws ServiceException;


    /**
     * Gets list of news for requested page.
     *
     * @param pageNumber requested page
     * @return list of news
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    List<News> readNewsListPerPage(long pageNumber) throws ServiceException;


    /**
     * Returns number of news.
     *
     * @return number of news
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    Long getNumberOfAllNews() throws ServiceException;


    /**
     * Creates bind between instance of <code>News</News>
     * and instance of <code>Author</code>.
     *
     * @param newsId ID of <code>News</code>
     * @param authorId ID of <code>Author</code>
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    void createBindWithAuthor(long newsId, long authorId) throws ServiceException;


    /**
     * Creates bind between instance of <code>News</News>
     * and instances of <code>Tag</code>.
     *
     * @param newsId ID <code>News</code>
     * @param tagIdList ID of <code>Tag</code> list
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    void createBindWithTags(long newsId, List<Long> tagIdList) throws ServiceException;


    /**
     * Removes bind between instance of <code>News</News>
     * and instances of <code>Author</code>.
     *
     * @param newsId ID of news
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    void deleteBindWithAuthor(long newsId) throws ServiceException;


    /**
     * Creates bind between instance of <code>News</News>
     * and instances of <code>Tag</code>.
     *
     * @param newsId ID <code>News</code>
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    void deleteBindWithTags(long newsId) throws ServiceException;



}
