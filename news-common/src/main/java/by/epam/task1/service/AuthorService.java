package by.epam.task1.service;

import by.epam.task1.domain.Author;
import by.epam.task1.domain.News;
import by.epam.task1.exception.ServiceException;

import java.util.List;


/**
 * interface <code>AuthorService</code> working with
 * instance of <code>Author</code> and describe
 * its behavior using Data Access layer implementations
 * of interface <code>AuthorDao</code>.
 *
 * @author Dzmitry Padvalnikau
 * @see by.epam.task1.dao.AuthorDAO
 * @see by.epam.task1.service.AuthorService
 * @see by.epam.task1.domain.Author
 */
public interface AuthorService extends CRUDGenericService<Author> {

    /**
     * Gets list of authors for requested page using
     * method of AuthorDAO interface.
     *
     * @param pageNumber requested number of page
     * @return list of authors
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    List<Author> readAuthorListPerPage(long pageNumber) throws ServiceException;


    /**
     * Gets list of all authors from database using method of
     * authorDAO interface.
     *
     * @return list of authors
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    List<Author> readAuthorList() throws ServiceException;


    /**
     * Gets list of authors for requested news
     * using method of AuthorDAO interface.
     *
     * @param newsId ID requested news
     * @return list of authors
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    Author readAuthorForNews(long newsId) throws ServiceException;


    /**
     * Sets current date and time to instance
     * of <code>Author</code>.
     *
     * @param author instance of <code>Author</code> to be changed
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    void makeExpired(Author author) throws ServiceException;

    Long getNumberOfAllAuthors() throws ServiceException;

    public long getPagesNumber() throws ServiceException;
}
