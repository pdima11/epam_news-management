package by.epam.task1.service.impl;

import by.epam.task1.dao.TagDAO;
import by.epam.task1.domain.News;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.DAOException;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.TagService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/15/2016.
 */
@Named
public class TagServiceImpl implements TagService {
    private static final int NUMBER_OF_ITEMS_PER_PAGE = 11;

    @Inject
    private TagDAO tagDAO;

    @Override
    public Long create(Tag tag) throws ServiceException {
        Long tagId;

        if (tag == null) {
            throw new ServiceException("Null pointer for Tag");
        }

        try {
            tagId = tagDAO.create(tag);
        } catch (DAOException e) {
            throw new ServiceException("Failed to create Tag", e);
        }

        return tagId;
    }

    @Override
    public Tag read(long tagId) throws ServiceException {
        Tag tag;

        try {
            tag = tagDAO.read(tagId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to read Tag", e);
        }

        return tag;
    }

    @Override
    public void update(Tag tag) throws ServiceException {
        if (tag == null) {
            throw new ServiceException("Null pointer for Tag");
        }

        try {
            tagDAO.update(tag);
        } catch (DAOException e) {
            throw new ServiceException("Failed to update Tag", e);
        }
    }

    @Override
    public void delete(long tagId) throws ServiceException {
        try {
            tagDAO.delete(tagId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to delete Tag", e);
        }
    }

    @Override
    public List<Tag> readTagsForNews(long newsId) throws ServiceException{
        List<Tag> tags;

        try {
            tags = tagDAO.readTagsForNews(newsId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to read Tag list for News", e);
        }
        return tags;
    }

    @Override
    public List<Tag> readTagListPerPage(long pageNumber) throws ServiceException {
        List<Tag> tags;

        long endPosition = (pageNumber * NUMBER_OF_ITEMS_PER_PAGE) + 1;
        long startPosition = endPosition - NUMBER_OF_ITEMS_PER_PAGE;

        try {
            tags = tagDAO.readTagListPerPage(startPosition, endPosition);
        } catch (DAOException e) {
            throw new ServiceException("Failed to read Tag list per page", e);
        }
        return tags;
    }

    @Override
    public List<Tag> readTagList() throws ServiceException {
        List<Tag> tags;
        try {
            tags = tagDAO.readTagList();
        } catch (DAOException e) {
            throw new ServiceException("Failed to read tag list", e);
        }

        return tags;
    }

    @Override
    public Long getNumberOfAllTags() throws ServiceException {
        Long numberOfNews;

        try {
            numberOfNews = tagDAO.getNumberOfAllTags();
        } catch (DAOException e) {
            throw new ServiceException("Failed to ", e);
        }

        return numberOfNews;
    }

    @Override
    public long getPagesNumber() throws ServiceException {
        long numberOfAllTags = getNumberOfAllTags();
        return (long) (Math.ceil((double) numberOfAllTags / NUMBER_OF_ITEMS_PER_PAGE));
    }
}
