package by.epam.task1.service.impl;

import by.epam.task1.dao.CommentDAO;
import by.epam.task1.domain.Comment;
import by.epam.task1.exception.DAOException;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.CommentService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/15/2016.
 */
@Named
public class CommentServiceImpl implements CommentService {
    @Inject
    private CommentDAO commentDAO;

    @Override
    public Long create(Comment comment) throws ServiceException {
        Long commentId;

        if (comment == null) {
            throw new ServiceException("Null pointer for Comment");
        }

        try {
            commentId = commentDAO.create(comment);
        } catch (DAOException e) {
            throw new ServiceException("Failed to create Comment ", e);
        }

        return commentId;
    }

    @Override
    public Comment read(long commentId) throws ServiceException {
        Comment comment;

        try {
            comment = commentDAO.read(commentId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to read Comment", e);
        }

        return comment;
    }

    @Override
    public void update(Comment comment) throws ServiceException {
        if (comment == null) {
            throw new ServiceException("Null pointer for Comment");
        }

        try {
            commentDAO.update(comment);
        } catch (DAOException e) {
            throw new ServiceException("Failed to ", e);
        }
    }

    @Override
    public void delete(long commentId) throws ServiceException {
        try {
            commentDAO.delete(commentId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to delete Comment", e);
        }
    }

    @Override
    public List<Comment> readCommentsForNews(long newsId) throws ServiceException {
        List<Comment> comments;

        try {
            comments = commentDAO.readCommentListForNews(newsId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to read Comment for News", e);
        }

        return comments;
    }

    @Override
    public void deleteCommentsForNews(long newsId) throws ServiceException {
        try {
            commentDAO.deleteCommentsForNews(newsId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to delete Comments fro News", e);
        }
    }

}
