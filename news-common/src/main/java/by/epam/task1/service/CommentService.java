package by.epam.task1.service;

import by.epam.task1.domain.Comment;
import by.epam.task1.domain.News;
import by.epam.task1.exception.ServiceException;

import java.util.List;


/**
 * interface <code>CommentService</code> working with
 * instance of <code>Comment</code> and describe
 * its behavior using Data Access layer implementations
 * of interface <code>CommentDao</code>.
 *
 * @author Dzmitry Padvalnikau
 * @see by.epam.task1.dao.CommentDAO
 * @see by.epam.task1.service.CRUDGenericService
 * @see by.epam.task1.domain.Comment
 */
public interface CommentService extends CRUDGenericService<Comment> {


    /**
     * Gets list of comments for requested news
     * using method of CommentDAO.
     *
     * @param newsId IDrequested news
     * @return list of comments
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    List<Comment> readCommentsForNews(long newsId) throws ServiceException;


    /**
     * Removes list of comments that bind
     * with instance of <code>News</code>.
     *
     * @param newsId ID of <code>News</code> that
     *             bind with comments
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    void deleteCommentsForNews(long newsId) throws ServiceException;
}
