package by.epam.task1.service;

import by.epam.task1.domain.News;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.ServiceException;

import java.util.List;

/**
 * interface <code>TagService</code> working with
 * instance of <code>Tag</code> and describe
 * its behavior using Data Access layer implementations
 * of interface <code>TagDao</code>.
 *
 * @author Dzmitry Padvalnikau
 * @see by.epam.task1.dao.TagDAO
 * @see by.epam.task1.service.CRUDGenericService
 * @see by.epam.task1.domain.Tag
 */
public interface TagService extends CRUDGenericService<Tag> {


    /**
     * Gets list of tags that bind with
     * instance of <code>News</code>.
     *
     * @param newsId ID of <code>News</code>
     * @return list of tags
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    List<Tag> readTagsForNews(long newsId) throws ServiceException;


    /**
     * Gets list of tags for requested page using
     * method of TagDAO interface.
     *
     * @param pageNumber requested number of page
     * @return list of tags
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    List<Tag> readTagListPerPage(long pageNumber) throws ServiceException;


    /**
     * Gets list of all authors from database using method of
     * TagDAO interface.
     *
     * @return list of tags
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    List<Tag> readTagList() throws ServiceException;

    Long getNumberOfAllTags() throws ServiceException;
    public long getPagesNumber() throws ServiceException;
}
