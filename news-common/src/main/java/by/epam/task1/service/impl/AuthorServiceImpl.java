package by.epam.task1.service.impl;

import by.epam.task1.dao.AuthorDAO;
import by.epam.task1.domain.Author;
import by.epam.task1.exception.DAOException;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.AuthorService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/15/2016.
 */
@Named
public class AuthorServiceImpl implements AuthorService {
    private static final int NUMBER_OF_ITEMS_PER_PAGE = 11;

    @Inject
    private AuthorDAO authorDAO;


    @Override
    public Long create(Author author) throws ServiceException {
        Long authorId;

        if (author == null) {
            throw new ServiceException("Null pointer for Author");
        }

        try {
            authorId = authorDAO.create(author);
        } catch (DAOException e) {
            throw new ServiceException("Failed to create Author", e);
        }

        return authorId;
    }

    @Override
    public Author read(long authorId) throws ServiceException {
        Author author;

        try {
            author = authorDAO.read(authorId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to read Author", e);
        }

        return author;
    }

    @Override
    public void update(Author author) throws ServiceException {

        if (author == null) {
            throw new ServiceException("Null pointer for Author");
        }

        try {
            authorDAO.update(author);
        } catch (DAOException e) {
            throw new ServiceException("Failed to update Author", e);
        }
    }

    @Override
    public void delete(long authorId) throws ServiceException {

        try {
            authorDAO.delete(authorId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to delete Author", e);
        }
    }

    @Override
    public List<Author> readAuthorListPerPage(long pageNumber) throws ServiceException {
        List<Author> authors;

        long endPosition = (pageNumber * NUMBER_OF_ITEMS_PER_PAGE) + 1;
        long startPosition = endPosition - NUMBER_OF_ITEMS_PER_PAGE;

        try {
            authors = authorDAO.readAuthorListPerPage(startPosition, endPosition);
        } catch (DAOException e) {
            throw new ServiceException("Failed to read Author list", e);
        }

        return authors;
    }

    @Override
    public List<Author> readAuthorList() throws ServiceException {
        List<Author> authors;

        try {
            authors = authorDAO.readAuthorList();
        } catch (DAOException e) {
            throw new ServiceException("Failed to read Author list", e);
        }

        return authors;
    }

    @Override
    public Author readAuthorForNews(long newsId) throws ServiceException {
        Author author;

        try {
            author = authorDAO.readAuthorForNews(newsId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to read Author for News", e);
        }

        return author;
    }

    @Override
    public void makeExpired(Author author) throws ServiceException {

        try {
            author.setExpiredDate(new Date());
            authorDAO.makeExpired(author);
        } catch (DAOException e) {
            throw new ServiceException("Failed to make Author expired", e);
        }
    }

    @Override
    public Long getNumberOfAllAuthors() throws ServiceException {
        Long numberOfAuthors;

        try {
            numberOfAuthors = authorDAO.getNumberOfAllAuthors();
        } catch (DAOException e) {
            throw new ServiceException("Failed to ", e);
        }

        return numberOfAuthors;
    }

    @Override
    public long getPagesNumber() throws ServiceException {
        long numberOfAllTags = getNumberOfAllAuthors();
        return  (long) (Math.ceil((double) numberOfAllTags / NUMBER_OF_ITEMS_PER_PAGE));
    }
}
