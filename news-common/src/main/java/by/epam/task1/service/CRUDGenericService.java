package by.epam.task1.service;

import by.epam.task1.exception.ServiceException;

/**
 * Generic interface <code>CRUDGenericDao</code> that implement
 * four basic C.R.U.D (Create, Read, Update, Delete) operations
 * for working with Data Access Layer and it is common implementation
 * for all Service.
 *
 * @param <Entity> the type of instance in this interface
 * @author Dmitry Podvalnikov
 */
public interface CRUDGenericService<Entity> {

    /**
     * Adds entity to database using method of
     * CRUDGenericDAO interface.
     *
     * @param entity item to be added to database
     * @return inserted entity ID
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    Long create(Entity entity) throws ServiceException;


    /**
     * Gets entity from database using method of
     * CRUDGenericDAO interface.
     *
     * @param entityId ID of the entity that to be return
     * @return entity at the specified ID in the specific implementation of Service
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    Entity read(long entityId) throws ServiceException;


    /**
     * Changes data of entity.
     *
     * @param entity instance that to be changed in database
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    void update(Entity entity) throws ServiceException;


    /**
     * Removes entity from database using method of
     * CRUDGenericDAO interface.
     *
     * @param entityId ID of the entity that to be removed
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    void delete(long entityId) throws ServiceException;
}
