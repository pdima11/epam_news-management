package by.epam.task1.service;

import by.epam.task1.domain.FullNewsTO;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.exception.ServiceException;

import java.util.List;

/**
 * interface <code>FullNewsService</code> working with
 * transfer object instance of <code>FullNewsTO</code>
 * and describe its behavior using Data Access layer
 * implementations of interfaces <code>AuthorDao</code>,
 * <code>TagDAO</code>, <code>CommentDAO</code> and
 * <code>NewsDAO</code>.
 *
 * @author Dzmitry Padvalnikau
 * @see by.epam.task1.dao.CommentDAO
 * @see by.epam.task1.service.CommentService
 * @see by.epam.task1.domain.Comment
 */
public interface FullNewsService {


    /**
     * Creates instances of <code>Author<code/>, <code>Tag</code>
     * and <code>News</code> using creating methods of
     * <code>AuthorService<code/>, <code>TagService</code>
     * and <code>NewsService</code> interfaces.
     *
     * @param fullNews instance of transfer object
     *                 <code>FullNewsTO</code> thant
     *                 contains instances of <code>News<code/>,
     *                 <code>Tag</code> and <code>News</code>
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    Long create(FullNewsTO fullNews) throws ServiceException;


    /**
     * Removes instances of <code>Author<code/>, <code>Tag</code>
     * <code>Comment</code> and <code>News</code> using deleting methods of
     * <code>AuthorService<code/>, <code>TagService</code>,
     * <code>CommentService</code> and <code>NewsService</code> interfaces.
     *
     * @param newsId instance of transfer object
     *                 <code>FullNewsTO</code> thant
     *                 contains instances of <code>News<code/>,
     *                 <code>Tag</code>, <code>Comment</code>
     *                 and <code>News</code>
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    void delete(Long newsId) throws ServiceException;


    /**
     * Gets instance of transfer object <code>FullNewsTO</code> thant
     * contains instances of <code>News<code/>, <code>Tag</code>,
     * <code>Comment</code> and <code>News</code>.
     *
     * @param newsId ID
     * @return instance of transfer object <code>FullNewsTO</code>
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    FullNewsTO read(Long newsId) throws ServiceException;


    /**
     * Gets list of instance of transfer object <code>FullNewsTO</code> thant
     * contains instances of <code>News<code/>, <code>Tag</code>,
     * <code>Comment</code> and <code>News</code> for requested page
     * and requested instance of <code>SearchCriteriaTO</code>.
     *
     *
     * @param pageNumber requested page
     * @param searchCriteria instance of <code>SearchCriteria</code>
     * @return list of instance <code>FullNewsTO</code>
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    List<FullNewsTO> search(long pageNumber, SearchCriteriaTO searchCriteria) throws ServiceException;


    /**
     * Gets list of instance of transfer object <code>FullNewsTO</code> thant
     * contains instances of <code>News<code/>, <code>Tag</code>,
     * <code>Comment</code> and <code>News</code> for requested page.
     *
     *
     * @param pageNumber requested page
     * @return list of instance <code>FullNewsTO</code>
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    List<FullNewsTO> readNewsList(long pageNumber) throws ServiceException;


    /**
     * Gets list of instance of transfer object <code>FullNewsTO</code> thant
     * contains instances of <code>News<code/>, <code>Tag</code>,
     * <code>Comment</code> and <code>News</code> that sorted by number of
     * comments for requested page.
     *
     *
     * @param pageNumber
     * @return list of instance <code>FullNewsTO</code>
     * @throws by.epam.task1.exception.ServiceException author-defined exception occurs when
     *         <code>DAOException</code> throw up
     * @see by.epam.task1.exception.DAOException
     */
    List<FullNewsTO> readNewsListSortedByComments(long pageNumber) throws ServiceException;

    void update(FullNewsTO fullNews) throws ServiceException;

    public long getPagesNumber() throws ServiceException;
}
