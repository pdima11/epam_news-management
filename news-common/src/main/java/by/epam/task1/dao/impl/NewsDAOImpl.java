package by.epam.task1.dao.impl;

import by.epam.task1.dao.NewsDAO;
import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.exception.DAOException;
import by.epam.task1.util.SearchCriteriaManager;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 13.02.2016.
 */
@Named
public class NewsDAOImpl implements NewsDAO {

    private static final String ORACLE_SQL_QUERY_CREATE = "INSERT INTO" +
            " news(title, short_text, full_text, creation_date, modification_date)" +
            " VALUES(?, ?, ?, CURRENT_TIMESTAMP, SYSDATE)";

    private static final String ORACLE_SQL_QUERY_READ = "SELECT" +
            " news_id, title, short_text, full_text, creation_date, modification_date" +
            " FROM news" +
            " WHERE news_id = ?";

    private static final String ORACLE_SQL_QUERY_UPDATE = "UPDATE news" +
            " SET title = ?, short_text = ?, full_text = ?, modification_date = SYSDATE" +
            " WHERE news_id = ?";

    private static final String ORACLE_SQL_QUERY_DELETE = "DELETE" +
            " FROM news " +
            " WHERE news_id = ?";

    private static final String ORACLE_SQL_QUERY_READ_LIST_SORTED_BY_COMMENTS = "SELECT" +
            " news_id, title, short_text, full_text, creation_date, modification_date" +
            "    FROM (SELECT all_news.*, ROWNUM row_num " +
            "          FROM (SELECT news_id, title, short_text, full_text, creation_date, news.modification_date" +
            "                FROM news" +
            "                ORDER BY (SELECT COUNT(comments.news_id)" +
            "                          FROM comments" +
            "                          WHERE news.news_id = comments.news_id) DESC, creation_date DESC) all_news" +
            "          WHERE ROWNUM < ?)" +
            "    WHERE row_num >= ?";

    private static final String ORACLE_SQL_QUERY_COUNT_ALL = "SELECT COUNT(*)" +
            " FROM news";

    private static final String ORACLE_SQL_QUERY_CREATE_BIND_WITH_AUTHOR = "INSERT INTO" +
            " news_authors(news_id, author_id)" +
            " VALUES (?, ?)";

    private static final String ORACLE_SQL_QUERY_CREATE_BIND_WITH_TAGS = "INSERT INTO" +
            " news_tags(news_id, tag_id)" +
            " VALUES (?, ?)";

    private static final String ORACLE_SQL_QUERY_DELETE_BIND_WITH_AUTHOR = "DELETE" +
            " FROM news_authors" +
            " WHERE news_id = ?";

    private static final String ORACLE_SQL_QUERY_DELETE_BIND_WITH_TAGS = "DELETE" +
            " FROM news_tags" +
            " WHERE news_id = ?";

    private static final String ORACLE_SQL_QUERY_READ_LIST_PER_PAGE = "SELECT" +
            " news_id, title, short_text, full_text, creation_date, modification_date" +
            " FROM (SELECT all_news.*, ROWNUM row_num " +
            "       FROM (SELECT news_id, title, short_text, full_text, creation_date, modification_date" +
            "             FROM news" +
            "             ORDER BY creation_date DESC) all_news" +
            "       WHERE ROWNUM < ?)" +
            " WHERE row_num >= ?";

    private static final String NEWS_ID_ATTRIBUTE = "news_id";

    @Inject
    private DataSource dataSource;


    @Override
    public Long create(News news) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_CREATE;
        Long newsId = null;
        String generatedKeys[] = { NEWS_ID_ATTRIBUTE };

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery, generatedKeys)) {

            statement.setNString(1, news.getTitle());
            statement.setNString(2, news.getShortText());
            statement.setNString(3, news.getFullText());

            statement.executeUpdate();

            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    newsId = resultSet.getLong(1);
                }
            }

            return newsId;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing News create operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public News read(long newsId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_READ;
        News news = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, newsId);

            try (ResultSet resultSet = statement.executeQuery()) {

                if (resultSet.next()) {
                    news = new News();
                    news.setId(resultSet.getLong(1));
                    news.setTitle(resultSet.getNString(2));
                    news.setShortText(resultSet.getNString(3));
                    news.setFullText(resultSet.getNString(4));
                    news.setCreationDate(resultSet.getTimestamp(5));
                    news.setModificationDate(resultSet.getDate(6));
                }
            }

            return news;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing News read operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(News news) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_UPDATE;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setNString(1, news.getTitle());
            statement.setNString(2, news.getShortText());
            statement.setNString(3, news.getFullText());
            statement.setLong(4, news.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing News update operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(long newsId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_DELETE;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, newsId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing News delete operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> search(long from, long to, SearchCriteriaTO searchCriteria) throws DAOException{
        String sqlQuery = SearchCriteriaManager.generateSQLQuery(searchCriteria);
        News news = null;
        List<News> newsList = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            statement.setLong(1, to);
            statement.setLong(2, from);

            try (ResultSet resultSet = statement.executeQuery()) {
                newsList = new ArrayList<>();
                while (resultSet.next()) {
                    news = new News();
                    news.setId(resultSet.getLong(1));
                    news.setTitle(resultSet.getNString(2));
                    news.setShortText(resultSet.getNString(3));
                    news.setFullText(resultSet.getNString(4));
                    news.setCreationDate(resultSet.getTimestamp(5));
                    news.setModificationDate(resultSet.getDate(6));

                    newsList.add(news);
                }
            }

            return newsList;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing search News list", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> readNewsListSortedByComments(long from, long to) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_READ_LIST_SORTED_BY_COMMENTS;
        News news = null;
        List<News> newsList = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, to);
            statement.setLong(2, from);

            try (ResultSet resultSet = statement.executeQuery()) {
                newsList = new ArrayList<>();
                while (resultSet.next()) {
                    news = new News();
                    news.setId(resultSet.getLong(1));
                    news.setTitle(resultSet.getNString(2));
                    news.setShortText(resultSet.getNString(3));
                    news.setFullText(resultSet.getNString(4));
                    news.setCreationDate(resultSet.getTimestamp(5));
                    news.setModificationDate(resultSet.getDate(6));

                    newsList.add(news);
                }
            }

            return newsList;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read News list sorted by comments", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    @Override
    public Long getNumberOfAllNews() throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_COUNT_ALL;
        Long newsCount = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    newsCount = resultSet.getLong(1);
                }
            }

            return newsCount;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing get number of News operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    @Override
    public void createBindNewsWithAuthor(long newsId, long authorId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_CREATE_BIND_WITH_AUTHOR;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, newsId);
            statement.setLong(2, authorId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing create bind News with Author operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }


    @Override
    public void createBindNewsWithTags(long newsId, List<Long> tagIdList) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_CREATE_BIND_WITH_TAGS;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            for (Long tagId: tagIdList) {
                statement.setLong(1, newsId);
                statement.setLong(2, tagId);
                statement.executeUpdate();
            }

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing create bind News with Tags operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteBindNewsWithAuthor(long newsId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_DELETE_BIND_WITH_AUTHOR;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, newsId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing delete bind News with Author operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }

    @Override
    public void deleteBindNewsWithTags(long newsId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_DELETE_BIND_WITH_TAGS;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing delete bind News with Tags operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> readNewsListPerPage(long from, long to) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_READ_LIST_PER_PAGE;
        News news = null;
        List<News> newsList = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, to);
            statement.setLong(2, from);

            try (ResultSet resultSet = statement.executeQuery()) {
                newsList = new ArrayList<>();
                while (resultSet.next()) {
                    news = new News();
                    news.setId(resultSet.getLong(1));
                    news.setTitle(resultSet.getNString(2));
                    news.setShortText(resultSet.getNString(3));
                    news.setFullText(resultSet.getNString(4));
                    news.setCreationDate(resultSet.getTimestamp(5));
                    news.setModificationDate(resultSet.getDate(6));

                    newsList.add(news);
                }
            }

            return newsList;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read News list per page operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


}
