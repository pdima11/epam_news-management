package by.epam.task1.dao;

import by.epam.task1.domain.Tag;
import by.epam.task1.exception.DAOException;

import java.util.List;


/**
 * Interface <code>TagDAO</code> of Data Access Layer
 * that implements methods <code>CRUDGenericDAO</code> and
 * describe other methods for working with database
 * and instance of <code>Tag</code>.
 *
 * @author Dmitry Podvalnikov
 * @see by.epam.task1.dao.CRUDGenericDAO
 * @see by.epam.task1.domain.Tag
 *
 */
public interface TagDAO extends CRUDGenericDAO<Tag> {


    /**
     * Removes ID of tags and ID of news list to database
     * table that stores binds between <code>News</code>
     * and <code>Tag</code>.
     *
     * @param tagId ID of <code>Tag</code> that
     *              need remove bind with instance of <code>News</code>
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see by.epam.task1.domain.News
     * @see by.epam.task1.domain.Tag
     * @see java.sql.SQLException
     */
    void deleteBindTagWithNews(long tagId) throws DAOException;

    /**
     * Finds and returns tags from database that
     * are associated with specific news.
     *
     * @param newsId ID of instance <code>News</code>
     *             for search tag list
     * @return tag list that was founded by instance of <code>News</code>
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see by.epam.task1.domain.News
     * @see java.sql.SQLException
     */
    List<Tag> readTagsForNews(long newsId) throws DAOException;


    /**
     * Returns part of tags that storage in database.
     *
     * @param from start position for reading from
     *             database table of tags
     * @param to end position for reading from
     *             database table of tags
     * @return tag list from database table in requested range
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    List<Tag> readTagListPerPage(long from, long to) throws DAOException;


    /**
     * Returns all tags that storage in database.
     *
     * @return all tags from database table
     * by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    List<Tag> readTagList() throws DAOException;


    Long getNumberOfAllTags() throws DAOException;
}
