package by.epam.task1.dao;

import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.exception.DAOException;

import java.util.List;

/**
 * Interface <code>NewsDAO</code> of Data Access Layer
 * that implements methods <code>CRUDGenericDAO</code> and
 * describe other methods for working with database
 * and instance of <code>News</code>.
 *
 * @author Dmitry Podvalnikov
 * @see by.epam.task1.dao.CRUDGenericDAO
 * @see by.epam.task1.domain.News
 *
 */
public interface NewsDAO extends CRUDGenericDAO<News> {


    /**
     * Returns list of news from database that
     * are associated with specific author and
     * tags that storage in instance of <code>SearchCriteria</code>.
     *
     * @param from start position in table of News from database
     *             for getting list of instance of <code>News</code>
     * @param to end position in table of News from database
     *             for getting list of instance of <code>News</code>
     * @param searchCriteria object which contains instance of
     *                       <code>Author</code> and list of
     *                       instance of <code>Tag</code>
     * @return news list from database table in requested range
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    List<News> search(long from, long to, SearchCriteriaTO searchCriteria) throws DAOException;


    /**
     * Returns list of news from database
     * that sorted by number of comments.
     *
     * @return sorted list of news
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    List<News> readNewsListSortedByComments(long from, long to) throws DAOException;


    /**
     * Returns number of news that stored in database.
     *
     * @return number of news
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    Long getNumberOfAllNews() throws DAOException;


    /**
     * Adds ID of news and ID of author to database
     * table that stores binds between <code>News</code>
     * and <code>Author</code>.
     *
     * @param newsId instance of <code>News</code> that
     *             need bind with instance of <code>Author</code>
     * @param authorId instance of <code>Author</code> that
     *             need bind with instance of <code>News</code>
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see by.epam.task1.domain.News
     * @see by.epam.task1.domain.Author
     * @see java.sql.SQLException
     */
    void createBindNewsWithAuthor(long newsId, long authorId) throws DAOException;


    /**
     * Adds ID of news and ID of tag list to database
     * table that stores binds between <code>News</code>
     * and <code>Tag</code>.
     *
     * @param newsId ID of <code>News</code> that
     *             need bind with instances of <code>Tag</code>
     * @param tagIdList instances of <code>Tag</code> that
     *             need bind with instance of <code>News</code>
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see by.epam.task1.domain.News
     * @see by.epam.task1.domain.Tag
     * @see java.sql.SQLException
     */
    void createBindNewsWithTags(long newsId, List<Long> tagIdList) throws DAOException;


    /**
     * Removes ID of news and ID of author to database
     * table that stores binds between <code>News</code>
     * and <code>Author</code>.
     *
     * @param newsId ID of <code>News</code> that
     *              need remove bind with instance of <code>Author</code>
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see by.epam.task1.domain.News
     * @see by.epam.task1.domain.Author
     * @see java.sql.SQLException
     */
    void deleteBindNewsWithAuthor(long newsId) throws DAOException;


    /**
     * Removes ID of news and ID of tag list to database
     * table that stores binds between <code>News</code>
     * and <code>Tag</code>.
     *
     * @param newsId ID of <code>News</code> that
     *             need remove bind with instances of <code>Tag</code>
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see by.epam.task1.domain.News
     * @see by.epam.task1.domain.Tag
     * @see java.sql.SQLException
     */
    void deleteBindNewsWithTags(long newsId) throws DAOException;


    /**
     * Returns part of news that storage in database.
     *
     * @param from start position for reading from
     *             database table of news
     * @param to end position for reading from
     *             database table of news
     * @return news list from database table in requested range
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    List<News> readNewsListPerPage(long from, long to) throws DAOException;
}
