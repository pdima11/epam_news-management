package by.epam.task1.dao.impl;

import by.epam.task1.dao.AuthorDAO;
import by.epam.task1.domain.Author;
import by.epam.task1.exception.DAOException;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by User on 13.02.2016.
 */
@Named
public class AuthorDAOImpl implements AuthorDAO {

    private static final String ORACLE_SQL_QUERY_CREATE = "INSERT INTO authors(author_name)" +
            " VALUES(?)";

    private static final String ORACLE_SQL_QUERY_READ = "SELECT author_id, author_name, expired" +
            " FROM authors" +
            " WHERE author_id = ?";

    private static final String ORACLE_SQL_QUERY_UPDATE = "UPDATE authors" +
            " SET author_name = ?" +
            " WHERE author_id = ?";

    private static final String ORACLE_SQL_QUERY_DELETE = "DELETE" +
            " FROM authors" +
            " WHERE author_id = ?";

    private static final String ORACLE_SQL_QUERY_READ_FOR_NEWS = "SELECT authors.author_id, authors.author_name, authors.expired" +
            " FROM authors" +
            " INNER JOIN news_authors" +
            " ON authors.author_id = news_authors.author_id" +
            " WHERE news_authors.news_id = ?";

    private static final String ORACLE_SQL_QUERY_READ_LIST = "SELECT author_id, author_name, expired" +
            " FROM authors" +
            " ORDER BY author_name";

    private static final String ORACLE_SQL_QUERY_READ_LIST_PER_PAGE = "SELECT author_id, author_name, expired" +
            " FROM (SELECT all_authors.*, ROWNUM row_num " +
            "       FROM (SELECT author_id, author_name, expired" +
            "             FROM authors" +
            "             ORDER BY author_name) all_authors" +
            "       WHERE ROWNUM < ?)" +
            " WHERE row_num >= ?";

    private static final String ORACLE_SQL_QUERY_MAKE_EXPIRED = "UPDATE authors" +
            " SET expired = ?" +
            " WHERE author_id = ?";

    private static final String ORACLE_SQL_QUERY_COUNT_ALL = "SELECT COUNT(*)" +
            " FROM authors";

    private static final String AUTHOR_ID_ATTRIBUTE = "author_id";

    @Inject
    private DataSource dataSource;

    @Override
    public Long create(Author author) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_CREATE;
        String generatedKeys[] = { AUTHOR_ID_ATTRIBUTE };

        Connection connection = DataSourceUtils.getConnection(dataSource);

        Long authorId = null;

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery, generatedKeys)) {

            statement.setNString(1, author.getName());
            statement.executeUpdate();

            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    authorId = resultSet.getLong(1);
                }
            }

            return authorId;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Author creation operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Author read(long authorId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_READ;
        Author author = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, authorId);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    author = new Author();
                    author.setId(resultSet.getLong(1));
                    author.setName(resultSet.getNString(2));
                    author.setExpiredDate(resultSet.getTimestamp(3));
                }
            }

            return author;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Author read operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(Author author) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_UPDATE;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)){

            statement.setNString(1, author.getName());
            statement.setLong(2, author.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Author update operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }

    @Override
    public void delete(long authorId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_DELETE;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, authorId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Author delete operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }

    @Override
    public void makeExpired(Author author) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_MAKE_EXPIRED;
        Timestamp authorExpireDate = new Timestamp(author.getExpiredDate().getTime());

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setTimestamp(1, authorExpireDate);
            statement.setLong(2, author.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing make Author expire operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    @Override
    public Author readAuthorForNews(long newsId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_READ_FOR_NEWS;
        Author author = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, newsId);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    author = new Author();
                    author.setId(resultSet.getLong(1));
                    author.setName(resultSet.getNString(2));
                    author.setExpiredDate(resultSet.getTimestamp(3));
                }
            }

            return author;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Author for News operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Author> readAuthorListPerPage(long from, long to) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_READ_LIST_PER_PAGE;
        Author author = null;
        List<Author> authors = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, to);
            statement.setLong(2, from);

            try (ResultSet resultSet = statement.executeQuery()) {
                authors = new ArrayList<>();
                while (resultSet.next()) {
                    author = new Author();
                    author.setId(resultSet.getLong(1));
                    author.setName(resultSet.getNString(2));
                    author.setExpiredDate(resultSet.getTimestamp(3));
                    authors.add(author);
                }
            }

            return authors;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Author list per page operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Author> readAuthorList() throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_READ_LIST;
        Author author = null;
        List<Author> authors = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            try (ResultSet resultSet = statement.executeQuery()) {
                authors = new ArrayList<>();
                while (resultSet.next()) {
                    author = new Author();
                    author.setId(resultSet.getLong(1));
                    author.setName(resultSet.getNString(2));
                    author.setExpiredDate(resultSet.getTimestamp(3));
                    authors.add(author);
                }
            }

            return authors;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Author list operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Long getNumberOfAllAuthors() throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_COUNT_ALL;
        Long newsCount = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    newsCount = resultSet.getLong(1);
                }
            }

            return newsCount;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing get number of Authors operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
