package by.epam.task1.dao;

import by.epam.task1.domain.Comment;
import by.epam.task1.domain.News;
import by.epam.task1.exception.DAOException;

import java.util.List;

/**
 * Interface <code>CommentDAO</code> of Data Access Layer
 * that implements methods <code>CRUDGenericDAO</code> and
 * describe other methods for working with database
 * and instance of <code>Comment</code>.
 *
 * @author Dmitry Podvalnikov
 * @see by.epam.task1.dao.CRUDGenericDAO
 * @see by.epam.task1.domain.Comment
 *
 */
public interface CommentDAO extends CRUDGenericDAO<Comment> {

    /**
     * Finds and returns comments from database that
     * are associated with specific news.
     *
     * @param newsId ID of instance <code>News</code>
     *             for search comment list
     * @return comment list that was founded by instance of <code>News</code>
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see by.epam.task1.domain.News
     * @see java.sql.SQLException
     */
    List<Comment> readCommentListForNews(long newsId) throws DAOException;

    /**
     * Removes comments from database that
     * are associated with specific news.
     *
     * @param newsId ID of instance <code>News</code>
     *             for deleting comment list
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see by.epam.task1.domain.News
     * @see java.sql.SQLException
     */
    void deleteCommentsForNews(long newsId) throws DAOException;
}
