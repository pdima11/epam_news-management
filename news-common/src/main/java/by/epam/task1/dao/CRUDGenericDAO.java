package by.epam.task1.dao;

import by.epam.task1.exception.DAOException;


/**
 * Generic interface <code>CRUDGenericDao</code> that implement
 * four basic C.R.U.D (Create, Read, Update, Delete) operations
 * for working with database and it is common implementation for all DAO.
 *
 * @param <Entity> the type Entity that stored in database
 * @author Dmitry Podvalnikov
 */
public interface CRUDGenericDAO<Entity> {

    /**
     * Adds new entity to database.
     *
     * @param entity entity to be added to database
     * @return inserted entity ID
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    Long create(Entity entity) throws DAOException;


    /**
     * Finds entity by ID and return her from database.
     *
     * @param entityId ID of the entity that to be return
     * @return entity at the specified ID in the specific implementation of DAO
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    Entity read(long entityId) throws DAOException;


    /**
     * Saves changed data of entity to database.
     *
     * @param entity entity to be updated in database
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    void update(Entity entity) throws DAOException;


    /**
     * Removes entity from database.
     *
     * @param entityId ID of the entity to remove
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    void delete(long entityId) throws DAOException;
}
