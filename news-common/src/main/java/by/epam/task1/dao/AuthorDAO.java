package by.epam.task1.dao;

import by.epam.task1.domain.Author;
import by.epam.task1.exception.DAOException;

import java.util.List;

/**
 * Interface <code>AuthorDAO</code> of Data Access Layer
 * that implements methods <code>CRUDGenericDAO</code> and
 * describe other methods for working with database
 * and instance of <code>Author</code>.
 *
 * @author Dmitry Podvalnikov
 * @see by.epam.task1.dao.CRUDGenericDAO
 * @see by.epam.task1.domain.Author
 *
 */
public interface AuthorDAO extends CRUDGenericDAO<Author> {

    /**
     * Saves to database changes date field for
     * instance of <code>Author</code>.
     *
     * @param author
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see by.epam.task1.domain.Author
     * @see java.sql.SQLException
     */
    void makeExpired(Author author) throws DAOException;

    /**
     * Finds and returns author from database that
     * are associated with specific news.
     *
     * @param newsId ID of instance <code>News</code>
     *             for search tag list
     * @return author that was founded by instance of <code>News</code>
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see by.epam.task1.domain.News
     * @see java.sql.SQLException
     */
    Author readAuthorForNews(long newsId) throws DAOException;


    /**
     * Returns part of authors that storage in database.
     *
     * @param from start position for reading from
     *             database table of authors
     * @param to end position for reading from
     *             database table of authors
     * @return author list from database table in requested range
     * @throws by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    List<Author> readAuthorListPerPage(long from, long to) throws DAOException;


    /**
     * Returns all authors that storage in database.
     *
     * @return all authors from database table
     * by.epam.task1.exception.DAOException author-defined exception occurs when
     *         <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    List<Author> readAuthorList() throws DAOException;

    Long getNumberOfAllAuthors() throws DAOException;
}
