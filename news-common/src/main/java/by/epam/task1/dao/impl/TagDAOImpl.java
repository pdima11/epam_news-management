package by.epam.task1.dao.impl;

import by.epam.task1.dao.TagDAO;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.DAOException;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/12/2016.
 */
@Named
public class TagDAOImpl implements TagDAO {

    private static final String ORACLE_SQL_QUERY_CREATE = "INSERT INTO tags(tag_name)" +
            " VALUES(?)";

    private static final String ORACLE_SQL_QUERY_READ = "SELECT tag_id, tag_name" +
            " FROM tags" +
            " WHERE tag_id = ?";

    private static final String ORACLE_SQL_QUERY_UPDATE = "UPDATE tags" +
            " SET tag_name = ?" +
            " WHERE tag_id = ?";

    private static final String ORACLE_SQL_QUERY_DELETE = "DELETE" +
            " FROM tags" +
            " WHERE tag_id = ?";

    private static final String ORACLE_SQL_QUERY_DELETE_BIND_WITH_NEWS = "DELETE" +
            " FROM news_tags" +
            " WHERE tag_id = ?";

    private static final String ORACLE_SQL_QUERY_READ_FOR_NEWS = "SELECT tags.tag_id, tags.tag_name" +
            " FROM tags" +
            " INNER JOIN news_tags" +
            " ON tags.tag_id = news_tags.tag_id" +
            " WHERE news_tags.news_id = ?";

    private static final String ORACLE_SQL_QUERY_READ_LIST = "SELECT tag_id, tag_name" +
            " FROM tags" +
            " ORDER BY tag_name";

    private static final String ORACLE_SQL_QUERY_READ_LIST_PER_PAGE = "SELECT tag_id, tag_name" +
            " FROM (SELECT all_tags.*, ROWNUM row_num" +
            "       FROM (SELECT tag_id, tag_name" +
            "             FROM tags" +
            "             ORDER BY tag_name) all_tags" +
            "       WHERE ROWNUM < ?)" +
            " WHERE row_num >= ?";

    private static final String ORACLE_SQL_QUERY_COUNT_ALL = "SELECT COUNT(*)" +
            " FROM tags";

    private static final String TAG_ID_ATTRIBUTE = "tag_id";

    @Inject
    private DataSource dataSource;

    @Override
    public Long create(Tag tag) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_CREATE;
        Long tagId = null;
        String generatedKeys[] = { TAG_ID_ATTRIBUTE };

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery, generatedKeys)) {

            statement.setNString(1, tag.getName());
            statement.executeUpdate();

            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    tagId = resultSet.getLong(1);
                }
            }

            return tagId;
        } catch (SQLException e) {
            throw new DAOException("an error occurred while executing Tag create operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Tag read(long tagId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_READ;
        Tag tag = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            statement.setLong(1, tagId);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    tag = new Tag();
                    tag.setId(resultSet.getLong(1));
                    tag.setName(resultSet.getNString(2));
                }
            }

            return tag;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Tag read operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(Tag tag) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_UPDATE;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setNString(1, tag.getName());
            statement.setLong(2, tag.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Tag update operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(long tagId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_DELETE;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, tagId);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Tag delete operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteBindTagWithNews(long tagId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_DELETE_BIND_WITH_NEWS;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, tagId);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing delete bind Tag with News operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }

    @Override
    public List<Tag> readTagsForNews(long newsId) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_READ_FOR_NEWS;
        Tag tag = null;
        List<Tag> tags = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, newsId);

            try (ResultSet resultSet = statement.executeQuery()) {
                tags = new ArrayList<>();
                while (resultSet.next()) {
                    tag = new Tag();
                    tag.setId(resultSet.getLong(1));
                    tag.setName(resultSet.getNString(2));

                    tags.add(tag);
                }
            }

            return tags;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Tags for News operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Tag> readTagListPerPage(long from, long to) throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_READ_LIST_PER_PAGE;
        Tag tag = null;
        List<Tag> tags = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            statement.setLong(1, to);
            statement.setLong(2, from);

            try (ResultSet resultSet = statement.executeQuery()) {
                tags = new ArrayList<>();
                while (resultSet.next()) {
                    tag = new Tag();
                    tag.setId(resultSet.getLong(1));
                    tag.setName(resultSet.getNString(2));

                    tags.add(tag);
                }
            }

            return tags;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Tag list per page operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Tag> readTagList() throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_READ_LIST;
        Tag tag = null;
        List<Tag> tags = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            try (ResultSet resultSet = statement.executeQuery()) {
                tags = new ArrayList<>();
                while (resultSet.next()) {
                    tag = new Tag();
                    tag.setId(resultSet.getLong(1));
                    tag.setName(resultSet.getNString(2));

                    tags.add(tag);
                }
            }

            return tags;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Tag list operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Long getNumberOfAllTags() throws DAOException {
        String sqlQuery = ORACLE_SQL_QUERY_COUNT_ALL;
        Long newsCount = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    newsCount = resultSet.getLong(1);
                }
            }

            return newsCount;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing get number of Tags operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


}
