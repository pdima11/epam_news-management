package by.epam.task1.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * Created by Dzmitry_Padvalnikau on 4/22/2016.
 */
public class ServiceProfiler {
    private static final Logger logger = LogManager.getLogger();

    public Object profile(ProceedingJoinPoint joinPoint) throws Throwable{
        Object returnObject;
        StringBuilder logMessage = new StringBuilder();

        addToLogExecutingMethod(logMessage, joinPoint);
        addToLogMethodsParameters(logMessage, joinPoint);

        try {
            returnObject =  joinPoint.proceed();
            addToLogReturnedObject(logMessage, returnObject);

            return returnObject;
        } catch (Throwable e) {
            addToLogThrownException(logMessage, e);
            throw e;
        } finally {
            logger.debug(logMessage.toString());
        }
    }

    private void addToLogExecutingMethod(StringBuilder sb, ProceedingJoinPoint joinPoint) {
        sb.append(joinPoint.toLongString());
        sb.append("\n");
    }

    private void addToLogMethodsParameters(StringBuilder sb, ProceedingJoinPoint joinPoint) {
        Object[] parameters = joinPoint.getArgs();

        if (parameters.length != 0) {
            sb.append("Parameters:\n");
            for (Object obj: parameters) {
                sb.append("\t");
                sb.append(obj.toString());
            }
            sb.append("\n");
        }
    }

    private void addToLogReturnedObject(StringBuilder sb, Object returnObject) {
        sb.append("Return:\n\t");
        if (returnObject != null) {
            sb.append(returnObject.toString());
        } else {
            sb.append("void");
        }
        sb.append("\n");
    }

    private void addToLogThrownException(StringBuilder sb, Throwable e) {
        sb.append("Exception:\n\t");
        sb.append(e);
        sb.append("\n");
    }

}
