package by.epam.task1.util;

import by.epam.task1.domain.SearchCriteriaTO;

import java.util.List;

public class SearchCriteriaManager {
    private static final String START_SQL_QUERY = "SELECT * FROM (SELECT all_news.*, ROWNUM row_num " +
          "FROM (SELECT news.news_id, title, short_text, full_text, news.creation_date, modification_date " +
          "FROM news ";
    private static final String JOIN_WITH_AUTHORS_TABLE = " INNER JOIN news_authors" +
            " ON news.news_id = news_authors.news_id";
    private static final String JOIN_WITH_TAGS_TABLE = " INNER JOIN news_tags" +
            " ON news.news_id = news_tags.news_id";
    private static final String END_SQL_QUERY = "ORDER BY creation_date DESC) all_news" +
            " WHERE ROWNUM < ?) WHERE row_num >= ?";


    public static String generateSQLQuery(SearchCriteriaTO searchCriteria) {
        Long authorId = searchCriteria.getAuthorId();
        List<Long> tagIdList = searchCriteria.getTagIdList();

        StringBuilder sb = new StringBuilder();
        sb.append(START_SQL_QUERY);

        if (authorId != null) {
            sb.append(JOIN_WITH_AUTHORS_TABLE);
        }

        if (tagIdList != null) {
            sb.append(JOIN_WITH_TAGS_TABLE);
        }

        String authorParam = getAuthorParametersForSearch(authorId);
        sb.append(authorParam);

        String tagsParam = getTagsParametersForSearch(tagIdList);
        sb.append(tagsParam);

        sb.append(END_SQL_QUERY);

        return sb.toString();
    }

    private static String getAuthorParametersForSearch(Long authorId) {
        if (authorId == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(" WHERE author_id = ");
        sb.append(authorId);

        return sb.toString();
    }

    private static String getTagsParametersForSearch(List<Long> tagIdList) {
        if (tagIdList == null || tagIdList.isEmpty()) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(" AND ( ");

        for (Long tagId: tagIdList) {
            sb.append(" tag_id = ");
            sb.append(tagId);
            sb.append(" OR ");
        }

        int lastPositionOR = sb.lastIndexOf("OR");
        sb.delete(lastPositionOR, lastPositionOR + 2);
        sb.append(") ");

        return sb.toString();
    }

}