-- create database for task1
CREATE TABLE authors (
    author_id NUMBER(20) NOT NULL,
    author_name NVARCHAR2(100) NOT NULL,
    expired TIMESTAMP,
    CONSTRAINT PK_AUTHOR PRIMARY KEY (author_id)
);
 
CREATE TABLE news (
    news_id NUMBER(20) NOT NULL,
    title NVARCHAR2(500) NOT NULL,
    short_text NVARCHAR2(1000) NOT NULL,
    full_text NVARCHAR2(2000) NOT NULL,
    creation_date TIMESTAMP NOT NULL,
    modification_date DATE NOT NULL,
    CONSTRAINT PK_NEWS PRIMARY KEY (news_id)
);

CREATE TABLE tags (
    tag_id NUMBER(20) NOT NULL,
    tag_name NVARCHAR2(50) NOT NULL,
    CONSTRAINT PK_TAG PRIMARY KEY (tag_id) 
);
 
CREATE TABLE comments (
    comment_id NUMBER(20) NOT NULL,
    news_id NUMBER(20) NOT NULL,
    comment_text NVARCHAR2(500) NOT NULL,
    creation_date TIMESTAMP NOT NULL,
    CONSTRAINT PK_COMMENT PRIMARY KEY (comment_id),
    CONSTRAINT FK_COMMENT_NEWS FOREIGN KEY (news_id) REFERENCES news(news_id)
);
 
CREATE TABLE news_tags (
    news_id NUMBER(20) NOT NULL,
    tag_id NUMBER(20) NOT NULL,
    CONSTRAINT FK_NEWS_TAG_NEWS FOREIGN KEY (news_id) REFERENCES news(news_id),
    CONSTRAINT FK_NEWS_TAG_TAG FOREIGN KEY (tag_id) REFERENCES tags(tag_id)
);
 
CREATE TABLE news_authors (
    news_id NUMBER(20) NOT NULL,
    author_id NUMBER(20) NOT NULL,
    CONSTRAINT FK_NEWS_AUTHOR_AUTHOR FOREIGN KEY (author_id) REFERENCES authors(author_id),
    CONSTRAINT FK_NEWS_AUTHOR_NEWS FOREIGN KEY (news_id) REFERENCES news(news_id)
);

CREATE TABLE users(
  user_id NUMBER(20) NOT NULL,
  user_name NVARCHAR2(100) NOT NULL,
  login VARCHAR2(100) NOT NULL,
  password VARCHAR2(32) NOT NULL,
  CONSTRAINT PK_USERS PRIMARY KEY(user_id)
);

CREATE TABLE roles(
  user_id NUMBER(20) NOT NULL,
  role_name VARCHAR2(50) NOT NULL,
  CONSTRAINT FK_USERS 
    FOREIGN KEY(user_id) 
    REFERENCES users(user_id)
);


